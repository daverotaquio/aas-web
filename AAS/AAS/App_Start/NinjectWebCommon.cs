[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(AAS.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(AAS.App_Start.NinjectWebCommon), "Stop")]

namespace AAS.App_Start
{
    using System;
    using System.Web;
    using AAS.ApplicationServices.Concrete;
    using AAS.ApplicationServices.Interface;
    using AAS.DataModels;
    using AAS.Persistence.Concrete;
    using AAS.Persistence.Interface;
    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;
    using Ninject.Web.Common.WebHost;

    public static class NinjectWebCommon
    {
        public static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IDataManager<ApplicationUser>>().To<DataManager<ApplicationUser>>();
            kernel.Bind<IDataManager<Student>>().To<DataManager<Student>>();
            kernel.Bind<IDataManager<Subject>>().To<DataManager<Subject>>();
            kernel.Bind<IDataManager<Course>>().To<DataManager<Course>>();
            kernel.Bind<IDataManager<Adviser>>().To<DataManager<Adviser>>();
            kernel.Bind<IDataManager<CurriculumItemPreRequisites>>().To<DataManager<CurriculumItemPreRequisites>>();
            kernel.Bind<IDataManager<CurriculumItem>>().To<DataManager<CurriculumItem>>();
            kernel.Bind<IDataManager<CurriculumVersion>>().To<DataManager<CurriculumVersion>>();
            kernel.Bind<IDataManager<StudentRecord>>().To<DataManager<StudentRecord>>();
            kernel.Bind<IDataManager<AcademicTerm>>().To<DataManager<AcademicTerm>>();
            kernel.Bind<IDataManager<Grade>>().To<DataManager<Grade>>();
            kernel.Bind<IDataManager<Enrollment>>().To<DataManager<Enrollment>>();
            kernel.Bind<IDataManager<AdvisedSubject>>().To<DataManager<AdvisedSubject>>();
            kernel.Bind<IDataManager<Schedule>>().To<DataManager<Schedule>>();
            kernel.Bind<IDataManager<Room>>().To<DataManager<Room>>();

            kernel.Bind<IService<ApplicationUser>>().To<Service<ApplicationUser>>();
            kernel.Bind<IService<Subject>>().To<Service<Subject>>();
            kernel.Bind<IService<Student>>().To<Service<Student>>();
            kernel.Bind<IService<Course>>().To<Service<Course>>();
            kernel.Bind<IService<Adviser>>().To<Service<Adviser>>();
            kernel.Bind<IService<CurriculumItemPreRequisites>>().To<Service<CurriculumItemPreRequisites>>();
            kernel.Bind<IService<CurriculumItem>>().To<Service<CurriculumItem>>();
            kernel.Bind<IService<CurriculumVersion>>().To<Service<CurriculumVersion>>();
            kernel.Bind<IService<StudentRecord>>().To<Service<StudentRecord>>();
            kernel.Bind<IService<AcademicTerm>>().To<Service<AcademicTerm>>();
            kernel.Bind<IService<Grade>>().To<Service<Grade>>();
            kernel.Bind<IService<Enrollment>>().To<Service<Enrollment>>();
            kernel.Bind<IService<AdvisedSubject>>().To<Service<AdvisedSubject>>();
            kernel.Bind<IService<Schedule>>().To<Service<Schedule>>();
            kernel.Bind<IService<Room>>().To<Service<Room>>();
        }
    }
}