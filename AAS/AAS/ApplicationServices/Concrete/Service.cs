﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.Persistence.Interface;
using Sentry;

namespace AAS.ApplicationServices.Concrete
{
    public class Service<T> : IService<T> where T : class
    {
        private readonly IDataManager<T> _dataManager;

        public Service(IDataManager<T> dataManager)
        {
            _dataManager = dataManager;
        }

        public ApplicationDbContext GetContext()
        {
            return _dataManager.GetContext();
        }

        public bool Add(T entity)
        {
            try
            {
                _dataManager.Add(entity);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureEvent(new SentryEvent(e));

                return false;
            }

            return true;
        }


        public bool AddRange(IEnumerable<T> entities)
        {
            try
            {
                _dataManager.AddRange(entities);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureEvent(new SentryEvent(e));

                return false;
            }

            return true;
        }

        public bool Delete(int id)
        {
            try
            {
                _dataManager.Remove(id);

                return true;
            }
            catch (Exception e)
            {
                SentrySdk.CaptureEvent(new SentryEvent(e));

                return false;
            }
        }

        public bool Delete(string id)
        {
            try
            {
                _dataManager.Remove(id);

                return true;
            }
            catch (Exception e)
            {
                SentrySdk.CaptureEvent(new SentryEvent(e));

                return false;
            }
        }

        public bool DeleteRange(Expression<Func<T, bool>> predicate)
        {
            try
            {
                _dataManager.RemoveRange(predicate);

                return true;
            }
            catch (Exception e)
            {
                SentrySdk.CaptureEvent(new SentryEvent(e));

                return false;
            }
        }

        public IEnumerable<T> GetAll()
        {
            var result = _dataManager.Get();

            return result;
        }

        public IEnumerable<T> GetAll(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties)
        {
            var result = _dataManager.Get(filter, orderBy, enableTracking, includeProperties);

            return result;
        }

        public IEnumerable<T> GetAllJoined(
            Expression<Func<T, bool>> filter = null,
            Expression<Func<T, T>> select = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties)
        {
            var result = _dataManager.GetJoined(filter, select, orderBy, enableTracking, includeProperties);

            return result;
        }

        public IEnumerable<T> DeferredGetAll(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties)
        {
            var result = _dataManager.DeferredGet(filter, orderBy, enableTracking, includeProperties);

            return result;
        }

        public T GetSingle(int id)
        {
            var entity = _dataManager.GetById(id);

            return entity;
        }

        public T GetSingle(string id)
        {
            var entity = _dataManager.GetById(id);

            return entity;
        }

        public T GetSingle(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includeProperties)
        {
            var result = _dataManager.Get(filter, null, false, includeProperties);

            var singleResult = result.FirstOrDefault();

            return singleResult;
        }

        public bool Update(T entity)
        {
            try
            {
                _dataManager.Update(entity);
            }
            catch (Exception e)
            {
                SentrySdk.CaptureEvent(new SentryEvent(e));

                return false;
            }

            return true;
        }
    }
}