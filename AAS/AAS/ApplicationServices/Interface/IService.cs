﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AAS.ApplicationServices.Interface
{
    public interface IService<T>
    {
        IEnumerable<T> GetAll();
        bool Add(T entity);
        T GetSingle(int id);
        bool Update(T entity);
        bool Delete(int id);
        bool Delete(string id);
        IEnumerable<T> GetAll(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties);
        T GetSingle(Expression<Func<T, bool>> filter = null, params Expression<Func<T, object>>[] includeProperties);
        T GetSingle(string id);
        bool AddRange(IEnumerable<T> entities);
        bool DeleteRange(Expression<Func<T, bool>> predicate);
        IEnumerable<T> DeferredGetAll(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, bool enableTracking = false, params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetAllJoined(Expression<Func<T, bool>> filter = null, Expression<Func<T, T>> select = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, bool enableTracking = false, params Expression<Func<T, object>>[] includeProperties);
        DataModels.ApplicationDbContext GetContext();
    }
}
