﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Enums;
using AAS.Helpers;
using AAS.Models;
using Mapster;

namespace AAS.Controllers
{
    [Authorize]
    public class AdvisedSubjectsController : BaseController
    {
        private readonly IService<AdvisedSubject> _advisedSubjectService;

        public AdvisedSubjectsController(IService<AdvisedSubject> advisedSubjectService)
        {
            _advisedSubjectService = advisedSubjectService;
        }

        public ActionResult Index()
        {
            GetCurrentUserRole();

            return View();
        }

        public PartialViewResult Create()
        {
            return PartialView("Form");
        }

        public PartialViewResult Edit(int id)
        {
            var advisedSubject = _advisedSubjectService.GetSingle(id);

            var advisedSubjectViewModel = advisedSubject.Adapt<AdvisedSubjectViewModel>();

            return PartialView("Form", advisedSubjectViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var advisedSubject = _advisedSubjectService.GetSingle(id);

            var advisedSubjectViewModel = advisedSubject.Adapt<AdvisedSubjectViewModel>();

            return PartialView("Delete", advisedSubjectViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(IEnumerable<int> ids)
        {
            var result = false;

            foreach (var id in ids)
            {
                result = _advisedSubjectService.Delete(id);
            }

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _advisedSubjectService.GetSingle(id);

            var advisedSubjectViewModel = result.Adapt<AdvisedSubjectViewModel>();

            return PartialView(advisedSubjectViewModel);
        }

        public JsonResult Save(AdvisedSubjectViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var currentAdvisedSubject = _advisedSubjectService.GetSingle(model.Id);

                var updatedAdvisedSubject = model.Adapt(currentAdvisedSubject);

                result = _advisedSubjectService.Update(updatedAdvisedSubject);
            }
            else
            {
                var advisedSubject = model.AdvisedSubjectIds.ToList().Select(x =>
                {
                    var mapped = model.Adapt<AdvisedSubject>();
                    mapped.CurriculumItemId = x;

                    return mapped;
                });

                result = _advisedSubjectService.AddRange(advisedSubject);
            }

            return Json(new { success = result });
        }

        public JsonResult Update(AlternativeSubjectsViewModel model)
        {
            var currentAdvisedSubject = _advisedSubjectService.GetSingle(model.AlternativeSubjectReferenceId);

            currentAdvisedSubject.CurriculumItemId = model.AlternativeSubjectCurriculumItemId;
            currentAdvisedSubject.PreferredSection = (Section)model.AlternativeSubjectSection;

            bool result = _advisedSubjectService.Update(currentAdvisedSubject);

            return Json(new { success = result });
        }

        public JsonResult List(int id)
        {
            var result = _advisedSubjectService.GetAll(x => x.StudentId == id, includeProperties: new Expression<Func<AdvisedSubject, object>>[] { x => x.CurriculumItem.Subject, x => x.CurriculumItem.PreRequisites, x => x.CurriculumItem.PreRequisites.Select(y => y.PreRequisite), x => x.CurriculumItem.Schedules.Select(y => y.Room), x => x.Enrollment, x => x.CurriculumItem.Course });

            var schedules = result.SelectMany(x => x.CurriculumItem.Schedules.Where(y => y.Section == x.PreferredSection));

            var advisedSubjects = result.Select(x => new AdvisedSubjectsDataTableModel {
                CurriculumItemId = x.CurriculumItemId,
                Id = x.Id,
                SubjectCode = x.CurriculumItem.Subject.SubjectCode,
                DescriptiveTitle = $"{x.CurriculumItem.Subject.DescriptiveTitle}{(x.CurriculumItem.Schedules.Any() ? $"<br/><br/>" : "")}" + string.Join("<br />", x.CurriculumItem.Schedules.Where(y => y.Section == x.PreferredSection).OrderBy(y => y.DayOfWeek).Select(y => {
                    return $"{y.DayOfWeek.GetValueOrDefault()} {y.StartTime} - {y.EndTime} {y.Room?.RoomName}";
                })),
                YearCourseSection = $"{(int)x.CurriculumItem.Year.GetValueOrDefault()} {x.CurriculumItem.Course.ShortDescription} - {x.PreferredSection}",
                Units = x.CurriculumItem.Subject.Units.ToString(),
                PreRequisites = x.CurriculumItem.PreRequisites != null && x.CurriculumItem.PreRequisites.Any() ? x.CurriculumItem.PreRequisites.Select(y => { return $"<span>{y.PreRequisite.SubjectCode}</span>"; }).Aggregate((a, b) => string.Join("<br />", a, b)) : "",
                HasConflictingSchedule = x.CurriculumItem.Schedules.Where(z => z.Section == x.PreferredSection).Select(z => {
                    var hasConflicts = schedules.Where(y => y.CurriculumItemId != x.CurriculumItemId).Select(y => {
                        var ciSchedStart = DateTime.Parse(z.StartTime).TimeOfDay;
                        var ciSchedEnd = DateTime.Parse(z.EndTime).TimeOfDay;
                        var schedStart = DateTime.Parse(y.StartTime).TimeOfDay;
                        var schedEnd = DateTime.Parse(y.EndTime).TimeOfDay;

                        var ciSchedStartIsBetweenSchedStartAndSchedEnd = ciSchedStart >= schedStart && ciSchedStart < schedEnd;
                        var ciSchedEndIsBetweenSchedStartAndSchedEnd = ciSchedEnd > schedStart && ciSchedEnd <= schedEnd;
                        var schedStartIsBetweenciSchedStartAndciSchedEnd = schedStart >= ciSchedStart && schedStart < ciSchedEnd;
                        var schedEndIsBetweenciSchedStartAndciSchedEnd = schedEnd > ciSchedStart && schedEnd <= ciSchedEnd;

                        var hasConflict = z.DayOfWeek == y.DayOfWeek && ((ciSchedStartIsBetweenSchedStartAndSchedEnd && ciSchedEndIsBetweenSchedStartAndSchedEnd) || (schedStartIsBetweenciSchedStartAndciSchedEnd && schedEndIsBetweenciSchedStartAndciSchedEnd));

                        return hasConflict;
                    }).Any(y => y.Equals(true));

                    return hasConflicts;
                }).Any(y => y.Equals(true)),
                ConflictingSubjects = x.CurriculumItem.Schedules.Where(z => z.Section == x.PreferredSection).SelectMany(z => {
                    var hasConflicts = schedules.Where(y => y.CurriculumItemId != x.CurriculumItemId).Where(y => {
                        var ciSchedStart = DateTime.Parse(z.StartTime).TimeOfDay;
                        var ciSchedEnd = DateTime.Parse(z.EndTime).TimeOfDay;
                        var schedStart = DateTime.Parse(y.StartTime).TimeOfDay;
                        var schedEnd = DateTime.Parse(y.EndTime).TimeOfDay;

                        var ciSchedStartIsBetweenSchedStartAndSchedEnd = ciSchedStart >= schedStart && ciSchedStart < schedEnd;
                        var ciSchedEndIsBetweenSchedStartAndSchedEnd = ciSchedEnd > schedStart && ciSchedEnd <= schedEnd;
                        var schedStartIsBetweenciSchedStartAndciSchedEnd = schedStart >= ciSchedStart && schedStart < ciSchedEnd;
                        var schedEndIsBetweenciSchedStartAndciSchedEnd = schedEnd > ciSchedStart && schedEnd <= ciSchedEnd;

                        var hasConflict = z.DayOfWeek == y.DayOfWeek && ((ciSchedStartIsBetweenSchedStartAndSchedEnd && ciSchedEndIsBetweenSchedStartAndSchedEnd) || (schedStartIsBetweenciSchedStartAndciSchedEnd && schedEndIsBetweenciSchedStartAndciSchedEnd));

                        return hasConflict;
                    });

                    return hasConflicts;
                }).DefaultIfEmpty().Select(y => { return y != null ? $"{y.CurriculumItem.Subject.SubjectCode} - {y.CurriculumItem.Subject.DescriptiveTitle}" : ""; })?.Distinct()?.Aggregate((a, b) => string.Join("<br />", a, b))
            });

            return Json(advisedSubjects.OrderBy(x => x.CurriculumItemId), JsonRequestBehavior.AllowGet);
        }
    }
}