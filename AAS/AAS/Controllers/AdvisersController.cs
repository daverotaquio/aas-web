﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Models;
using Mapster;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace AAS.Controllers
{
    [Authorize]
    public class AdvisersController : BaseController
    {
        private readonly IService<Adviser> _adviserService;
        private readonly IService<Course> _courseService;

        public AdvisersController(
            IService<Adviser> adviserService,
            IService<Course> courseService)
        {
            _adviserService = adviserService;
            _courseService = courseService;
        }

        public ActionResult Index()
        {
            GetCurrentUserRole();

            return View();
        }

        public PartialViewResult Create()
        {
            var adviserViewModel = new AdviserViewModel();

            adviserViewModel.Courses = new SelectList(_courseService.GetAll(), "Id", "Name");

            return PartialView("Form", adviserViewModel);
        }

        public PartialViewResult Edit(int id)
        {
            var adviser = _adviserService.GetSingle(x => x.Id == id, includeProperties: x => x.User);

            var adviserViewModel = adviser.Adapt<AdviserViewModel>();

            adviserViewModel.Courses = new SelectList(_courseService.GetAll(), "Id", "Name", adviserViewModel.CourseId.GetValueOrDefault());

            return PartialView("Form", adviserViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var adviser = _adviserService.GetSingle(id);

            var adviserViewModel = adviser.Adapt<AdviserViewModel>();

            return PartialView("Delete", adviserViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(AdviserViewModel model)
        {
            var result = _adviserService.Delete(model.Id);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _adviserService.GetSingle(id);

            var adviserViewModel = result.Adapt<AdviserViewModel>();

            return PartialView(adviserViewModel);
        }

        public async Task<JsonResult> Save(AdviserViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var currentUser = await userManager.FindByIdAsync(model.UserId);

                currentUser.Email = model.User.Email;
                currentUser.UserName = model.User.Email;

                var userResult = await userManager.UpdateAsync(currentUser);

                if (userResult.Succeeded)
                {
                    var currentAdviser = _adviserService.GetSingle(model.Id);

                    var updatedAdviser = model.Adapt(currentAdviser);
                    updatedAdviser.User = null;

                    result = _adviserService.Update(updatedAdviser);
                }
                else
                {
                    result = false;
                }

            }
            else
            {
                var user = new ApplicationUser { UserName = model.User.Email, Email = model.User.Email };

                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var userManagerResult = await userManager.CreateAsync(user, "12345678");

                if (userManagerResult.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Adviser");

                    model.UserId = user.Id;

                    var adviser = model.Adapt<Adviser>();
                    adviser.User = null;

                    result = _adviserService.Add(adviser);
                }
                else
                {
                    result = false;
                }
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var result = _adviserService.GetAll(x => x.User.Roles.Any(y => y.RoleId == "bff886b4-8645-4bdb-b19e-e5fa85ca11ff"), includeProperties: new System.Linq.Expressions.Expression<Func<Adviser, object>>[] { x => x.User, x => x.Course }).OrderByDescending(x => x.Id);

            var advisers = result.Adapt<IEnumerable<AdviserViewModel>>();

            var adviserDataTableModel = advisers.Select(x => new AdviserDataTableModel
            {
                Id = x.Id,
                FullName = x.FullName,
                Email = x.User.Email,
                ContactNumber = x.ContactNumber,
                Course = x.Course?.Name
            });

            return Json(adviserDataTableModel, JsonRequestBehavior.AllowGet);
        }
    }
}