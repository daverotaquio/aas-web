﻿using AAS.App_Start;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace AAS.Controllers
{
    public class BaseController : Controller
    {
        public string GetCurrentUserRole()
        {
            var userService = NinjectWebCommon.bootstrapper.Kernel.Get<IService<ApplicationUser>>();

            var currentUserId = User.Identity.GetUserId();

            var currentUser = userService.GetSingle(x => x.Id == currentUserId, includeProperties: x => x.Roles);

            var context = userService.GetContext();

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            //var roleToRemove = roleMngr.Roles.FirstOrDefault(x => x.Name == "Student");

            var roles = roleMngr.Roles.ToList();
            //roles.Remove(roleToRemove);

            var role = roles.FirstOrDefault(x => x.Id == currentUser.Roles.FirstOrDefault()?.RoleId)?.Name;

            ViewBag.CurrentUserRole = role;

            return role;
        }
    }
}