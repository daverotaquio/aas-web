﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.Models;
using Mapster;

namespace AAS.Controllers
{
    [Authorize]
    public class CoursesController : BaseController
    {
        private readonly IService<Course> _courseService;

        public CoursesController(IService<Course> courseService)
        {
            _courseService = courseService;
        }

        public ActionResult Index()
        {
            GetCurrentUserRole();

            return View();
        }

        public PartialViewResult Create()
        {
            return PartialView("Form");
        }

        public PartialViewResult Edit(int id)
        {
            var course = _courseService.GetSingle(id);

            var courseViewModel = course.Adapt<CourseViewModel>();

            return PartialView("Form", courseViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var course = _courseService.GetSingle(id);

            var courseViewModel = course.Adapt<CourseViewModel>();

            return PartialView("Delete", courseViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(CourseViewModel model)
        {
            var result = _courseService.Delete(model.Id);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _courseService.GetSingle(id);

            var courseViewModel = result.Adapt<CourseViewModel>();

            return PartialView(courseViewModel);
        }

        public JsonResult Save(CourseViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var currentCourse = _courseService.GetSingle(model.Id);

                var updatedCourse = model.Adapt(currentCourse);

                result = _courseService.Update(updatedCourse);
            }
            else
            {
                var course = model.Adapt<Course>();

                result = _courseService.Add(course);
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var result = _courseService.GetAll().OrderByDescending(x => x.Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}