﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Models;
using Mapster;

namespace AAS.Controllers
{
    [Authorize]
    public class CurriculumItemsController : BaseController
    {
        private readonly IService<CurriculumItem> _curriculumItemService;
        private readonly IService<Subject> _subjectService;
        private readonly IService<Course> _courseService;
        private readonly IService<CurriculumVersion> _curriculumVersionService;
        private readonly IService<CurriculumItemPreRequisites> _curriculumItemPreRequisiteService;
        private readonly IService<Adviser> _adviserService;
        private readonly IService<Schedule> _scheduleService;
        private readonly IService<Room> _roomService;
        private readonly IService<AdvisedSubject> _advisedSubjectService;

        public CurriculumItemsController(
            IService<CurriculumItem> curriculumItemService,
            IService<Subject> subjectService,
            IService<Course> courseService,
            IService<CurriculumVersion> curriculumVersionService,
            IService<CurriculumItemPreRequisites> curriculumItemPreRequisiteService,
            IService<Adviser> adviserService,
            IService<Schedule> scheduleService,
            IService<Room> roomService,
            IService<AdvisedSubject> advisedSubjectService
            )
        {
            _curriculumItemService = curriculumItemService;
            _subjectService = subjectService;
            _courseService = courseService;
            _curriculumVersionService = curriculumVersionService;
            _curriculumItemPreRequisiteService = curriculumItemPreRequisiteService;
            _adviserService = adviserService;
            _scheduleService = scheduleService;
            _roomService = roomService;
            _advisedSubjectService = advisedSubjectService;
        }

        public ActionResult Index()
        {
            var currentUserRole = GetCurrentUserRole();

            if (currentUserRole != "Registrar")
            {
                ViewBag.IsUnauthorized = true;
                ViewBag.IsUnauthorizedCreate = true;
                ViewBag.IsUnauthorizedEdit = true;
            }

            return View();
        }

        public PartialViewResult Create()
        {
            var curriculumItemViewModel = new CurriculumItemViewModel();


            var rooms = _roomService.GetAll();

            var roomsViewModel = rooms.Adapt<IEnumerable<RoomViewModel>>();

            curriculumItemViewModel.Subjects = Enumerable.Empty<SelectListItem>();
            curriculumItemViewModel.Courses = Enumerable.Empty<SelectListItem>();
            curriculumItemViewModel.CurriculumVersions = Enumerable.Empty<SelectListItem>();
            curriculumItemViewModel.PreRequisite = Enumerable.Empty<SelectListItem>();
            curriculumItemViewModel.Advisers = Enumerable.Empty<SelectListItem>();
            curriculumItemViewModel.Schedules = Enumerable.Empty<ScheduleViewModel>().ToList();
            curriculumItemViewModel.Rooms = new SelectList(roomsViewModel, "RoomId", "RoomName");

            return PartialView("Form", curriculumItemViewModel);
        }

        public PartialViewResult Edit(int id)
        {
            var curriculumItem = _curriculumItemService.GetSingle(id);

            var curriculumItemViewModel = curriculumItem.Adapt<CurriculumItemViewModel>();

            var subjects = _subjectService.GetAll();

            var subjectsViewModel = subjects.Adapt<IEnumerable<SubjectViewModel>>();

            var preRequisites = _curriculumItemPreRequisiteService.GetAll(x => x.CurriculumItemtId == id).Select(x => x.PreRequisiteId);

            var courses = _courseService.GetAll();

            var coursesViewModel = courses.Adapt<IEnumerable<CourseViewModel>>();

            var curriculumVersions = _curriculumVersionService.GetAll();

            var curriculumVersionsViewModel = curriculumVersions.Adapt<IEnumerable<CurriculumVersionViewModel>>();

            var advisers = _adviserService.GetAll();

            var advisersViewModel = advisers.Adapt<IEnumerable<AdviserViewModel>>();

            var schedules = _scheduleService.GetAll(x => x.CurriculumItemId == id);

            var schedulesViewModel = schedules.Adapt<IEnumerable<ScheduleViewModel>>().ToList();

            var rooms = _roomService.GetAll();

            var roomsViewModel = rooms.Adapt<IEnumerable<RoomViewModel>>().OrderBy(x => x.RoomName);

            curriculumItemViewModel.Subjects = new SelectList(subjectsViewModel, "Id", "FullName", curriculumItem.SubjectId);
            curriculumItemViewModel.Courses = new SelectList(coursesViewModel, "Id", "Name", curriculumItem.CourseId);
            curriculumItemViewModel.CurriculumVersions = new SelectList(curriculumVersionsViewModel, "Id", "ShortDescription", curriculumItem.CurriculumVersionId);
            curriculumItemViewModel.PreRequisite = new MultiSelectList(subjectsViewModel, "Id", "FullName", preRequisites);
            curriculumItemViewModel.Advisers = new SelectList(advisersViewModel, "Id", "FullName", curriculumItem.AdviserId);
            curriculumItemViewModel.Rooms = new SelectList(roomsViewModel, "RoomId", "RoomName");

            schedulesViewModel.ForEach(x => { x.Rooms = new SelectList(roomsViewModel, "RoomId", "RoomName", x.RoomId); });

            curriculumItemViewModel.Schedules = schedulesViewModel.OrderBy(x => x.Section).ThenBy(x => x.DayOfWeek).ToList() ?? Enumerable.Empty<ScheduleViewModel>().ToList();

            return PartialView("Form", curriculumItemViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var curriculumItem = _curriculumItemService.GetSingle(x => x.Id == id, includeProperties: x => x.Subject);

            var curriculumItemViewModel = curriculumItem.Adapt<CurriculumItemViewModel>();

            return PartialView("Delete", curriculumItemViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(CurriculumItemViewModel model)
        {
            var preRequisiteResult = _curriculumItemPreRequisiteService.DeleteRange(x => x.CurriculumItemtId == model.Id);

            var curriculumItemresult = _curriculumItemService.Delete(model.Id);

            var result = curriculumItemresult && preRequisiteResult;

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _curriculumItemService.GetSingle(id);

            var curriculumItemViewModel = result.Adapt<CurriculumItemViewModel>();

            return PartialView(curriculumItemViewModel);
        }

        public JsonResult Save(CurriculumItemViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var currentCurriculumItem = _curriculumItemService.GetSingle(model.Id);

                var updatedCurriculumItem = model.Adapt(currentCurriculumItem);

                var curriculumItemResult = _curriculumItemService.Update(updatedCurriculumItem);


                if (curriculumItemResult)
                {
                    var curriculumItemResults = false;
                    var subjectsResults = false;

                    if (model.PreRequisiteIds != null)
                    {
                        _curriculumItemPreRequisiteService.DeleteRange(x => x.CurriculumItemtId == updatedCurriculumItem.Id);

                        var newCurriculumItemPreRequisite = model.PreRequisiteIds?.Select(x => new CurriculumItemPreRequisites
                        {
                            CurriculumItemtId = updatedCurriculumItem.Id,
                            PreRequisiteId = x.GetValueOrDefault()
                        });

                        curriculumItemResults = _curriculumItemPreRequisiteService.AddRange(newCurriculumItemPreRequisite);
                    }
                    else
                    {
                        curriculumItemResults = _curriculumItemPreRequisiteService.DeleteRange(x => x.CurriculumItemtId == updatedCurriculumItem.Id);
                    }

                    if (model.Schedules != null)
                    {
                        _scheduleService.DeleteRange(x => x.CurriculumItemId == updatedCurriculumItem.Id);

                        var newSchedules = model.Schedules?.Select(x => x.Adapt<Schedule>()).ToList();

                        newSchedules.ForEach(x => x.CurriculumItemId = model.Id);

                        subjectsResults = _scheduleService.AddRange(newSchedules);
                        subjectsResults = true;
                    }
                    else
                    {
                        subjectsResults = _scheduleService.DeleteRange(x => x.CurriculumItemId == updatedCurriculumItem.Id);
                    }

                    result = curriculumItemResult && curriculumItemResults && subjectsResults;
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                var curriculumItem = model.Adapt<CurriculumItem>();

                var subjectResult = _curriculumItemService.Add(curriculumItem);

                if (subjectResult && model.PreRequisiteIds != null)
                {
                    var curriculumItemPreRequisite = model.PreRequisiteIds?.Select(x => new CurriculumItemPreRequisites
                    {
                        CurriculumItemtId = curriculumItem.Id,
                        PreRequisiteId = x.GetValueOrDefault()
                    });

                    var subjectSpecificationResults = _curriculumItemPreRequisiteService.AddRange(curriculumItemPreRequisite);

                    result = subjectResult && subjectSpecificationResults;
                }
                else
                {
                    result = subjectResult;
                }
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var result = _curriculumItemService.GetAll(includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject, y => y.Course, z => z.CurriculumVersion }).OrderByDescending(x => x.Id);

            var curriculumItemsViewModel = result.Adapt<IEnumerable<CurriculumItemViewModel>>();

            return Json(curriculumItemsViewModel, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AlternativeSubjects(int id)
        {
            var advisedSubject = _advisedSubjectService.GetSingle(id);

            var advisedSubjectViewModel = advisedSubject.Adapt<AdvisedSubjectViewModel>();

            return PartialView(advisedSubjectViewModel);
        }

        public JsonResult AlternativeSubjectsList(int id)
        {
            var advisedSubject = _advisedSubjectService.GetSingle(id);

            var curriculumItemId = advisedSubject.CurriculumItemId;

            var curriculumItem = _curriculumItemService.GetSingle(curriculumItemId);

            var subjectId = curriculumItem.SubjectId;

            var result = _curriculumItemService.GetAll(x => x.SubjectId == subjectId, includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject, y => y.Course, z => z.CurriculumVersion, x => x.PreRequisites, x => x.Schedules.Select(y => y.Room) });

            var schedules = result.SelectMany(x => x.Schedules);

            var alternativeSubjects = new List<AdvisedSubjectsDataTableModel>();

            foreach (var item in result)
            {

                foreach (var section in item.Schedules.Select(x => x.Section).Distinct())
                {
                    if (advisedSubject.CurriculumItemId == item.Id && advisedSubject.PreferredSection == section) continue;

                    var alternativeSubject = new AdvisedSubjectsDataTableModel
                    {
                        CurriculumItemId = item.Id,
                        Id = id,
                        SubjectCode = item.Subject.SubjectCode,
                        DescriptiveTitle = $"{item.Subject.DescriptiveTitle}<br/><br/>" + string.Join("<br />", item.Schedules.Where(x => x.Section == section).OrderBy(y => y.DayOfWeek).Select(y => {
                            return $"{y.DayOfWeek.GetValueOrDefault()} {y.StartTime} - {y.EndTime} {y.Room.RoomName}";
                        })),
                        YearCourseSection = $"{(int)item.Year.GetValueOrDefault()} {item.Course.ShortDescription} - {section}",
                        Units = item.Subject.Units.ToString(),
                        PreRequisites = item.PreRequisites != null && item.PreRequisites.Any() ? item.PreRequisites.Select(y => { return $"<span>{y.PreRequisite.SubjectCode}</span>"; }).Aggregate((a, b) => string.Join("<br />", a, b)) : "",
                        Section = section.GetValueOrDefault()
                    };

                    alternativeSubjects.Add(alternativeSubject);
                }
            }

            return Json(alternativeSubjects, JsonRequestBehavior.AllowGet);
        }


        public JsonResult Subjects(string q)
        {
            var subjects = GetSubjects(q);

            return Json(subjects, JsonRequestBehavior.AllowGet);
        }

        public JsonResult PreRequisites(string q)
        {
            var subjects = GetSubjects(q);

            return Json(subjects, JsonRequestBehavior.AllowGet);
        }

        private IEnumerable<SubjectViewModel> GetSubjects(string query)
        {
            var subjects = string.IsNullOrEmpty(query) ? _subjectService.GetAll() : _subjectService.GetAll(x => x.DescriptiveTitle.Contains(query) || x.SubjectCode.Contains(query));

            var subjectsViewModel = subjects.Adapt<IEnumerable<SubjectViewModel>>();

            return subjectsViewModel;
        }

        public JsonResult Courses(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _courseService.GetAll() : _courseService.GetAll(x => x.Name.Contains(q));

            var courses = result.Adapt<IEnumerable<CourseViewModel>>();

            return Json(courses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Advisers(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _adviserService.GetAll() : _adviserService.GetAll(x => x.FirstName.Contains(q) || x.MiddleName.Contains(q) || x.LastName.Contains(q));

            var advisers = result.Adapt<IEnumerable<AdviserViewModel>>();

            return Json(advisers, JsonRequestBehavior.AllowGet);
        }

        public JsonResult Rooms(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _roomService.GetAll() : _roomService.GetAll(x => x.RoomName.Contains(q));

            var rooms = result.Adapt<IEnumerable<RoomViewModel>>();

            return Json(rooms, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurriculumVersions(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _curriculumVersionService.GetAll() : _curriculumVersionService.GetAll(x => x.ShortDescription.Contains(q) || x.LongDescription.Contains(q));

            var curriculumVersions = result.Adapt<IEnumerable<CurriculumVersionViewModel>>();

            return Json(curriculumVersions, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult ScheduleForm()
        {
            return PartialView();
        }
    }
}