﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.Helpers;
using AAS.Models;
using Mapster;
using Rotativa;
using Sentry;

namespace AAS.Controllers
{
    public class DocumentController : Controller
    {
        private readonly IService<Student> _studentService;
        private readonly IService<AdvisedSubject> _advisedSubjectService;

        public DocumentController(
            IService<Student> studentService,
            IService<AdvisedSubject> advisedSubjectService
            )
        {
            _studentService = studentService;
            _advisedSubjectService = advisedSubjectService;
        }

        public ActionResult GenerateTrialProgramPDF(int id)
        {
            var trialProgramViewModel = new TrialProgramViewModel();

            var student = _studentService.GetSingle(x => x.Id == id, includeProperties: new Expression<Func<Student, object>>[] { x => x.Enrollments.Select(y => y.AcademicTerm.SchoolYear), x => x.Enrollments.Select(y => y.Adviser), x => x.Course });

            var studentViewModel = student.Adapt<StudentViewModel>();

            var advisedSubjects = _advisedSubjectService.GetAll(x => x.StudentId == id, includeProperties: new Expression<Func<AdvisedSubject, object>>[] { x => x.CurriculumItem.Subject, x => x.CurriculumItem.PreRequisites.Select(y => y.PreRequisite), x => x.CurriculumItem.Schedules.Select(y => y.Room), x => x.Enrollment, x => x.CurriculumItem.Course });

            var advisedSubjectsViewModel = advisedSubjects.Adapt<IEnumerable<AdvisedSubjectViewModel>>();

            var schedules = advisedSubjects.SelectMany(x => x.CurriculumItem.Schedules.Where(y => y.Section == x.PreferredSection));

            var newSchedulesViewModel = new List<ScheduleViewModel>();

            SentrySdk.CaptureEvent(new SentryEvent{ Message = "Before Iteration" });

            for (int i = 0; i < schedules.Count(); i++)
            {
                if (i > 0)
                {
                    if (schedules.ElementAt(i - 1).CurriculumItemId == schedules.ElementAt(i).CurriculumItemId && schedules.ElementAt(i - 1).StartTime == schedules.ElementAt(i).StartTime && schedules.ElementAt(i - 1).EndTime == schedules.ElementAt(i).EndTime)
                    {
                        var currentScheduleViewModel = schedules.ElementAt(i);
                        var previousScheduleViewModel = newSchedulesViewModel.FirstOrDefault(x => x.CurriculumItemId == currentScheduleViewModel.CurriculumItemId);

                        previousScheduleViewModel.Days.Add(currentScheduleViewModel.DayOfWeek.GetValueOrDefault());
                    }
                    else
                    {
                        var scheduleViewModel = schedules.ElementAt(i).Adapt<ScheduleViewModel>();

                        scheduleViewModel.Days = new List<DayOfWeek>();
                        scheduleViewModel.Days.Add(scheduleViewModel.DayOfWeek.GetValueOrDefault());
                        scheduleViewModel.DayOfWeek = null;

                        newSchedulesViewModel.Add(scheduleViewModel);
                    }
                }
                else
                {
                    var scheduleViewModel = schedules.ElementAt(i).Adapt<ScheduleViewModel>();

                    scheduleViewModel.Days = new List<DayOfWeek>();
                    scheduleViewModel.Days.Add(scheduleViewModel.DayOfWeek.GetValueOrDefault());
                    scheduleViewModel.DayOfWeek = null;

                    newSchedulesViewModel.Add(scheduleViewModel);
                }

                SentrySdk.CaptureEvent(new SentryEvent { Message = $"Iteration {i}" });
            }

            SentrySdk.CaptureEvent(new SentryEvent { Message = "After Iteration" });

            trialProgramViewModel.IdNumber = student.IDNumber;
            trialProgramViewModel.FullName = studentViewModel.FullName;
            trialProgramViewModel.FirstName = studentViewModel.FirstName;
            trialProgramViewModel.MiddleName = studentViewModel.MiddleName;
            trialProgramViewModel.LastName = studentViewModel.LastName;
            trialProgramViewModel.Adviser = studentViewModel.Enrollments.FirstOrDefault(x => x.EnrollmentStatus.GetValueOrDefault() == Enums.EnrollmentStatus.Active)?.Adviser?.FullName;
            trialProgramViewModel.YearCourseSection = $"{(int)student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == Enums.EnrollmentStatus.Active)?.Year} {student.Course.ShortDescription} {student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == Enums.EnrollmentStatus.Active)?.Section}";
            trialProgramViewModel.Schedules = newSchedulesViewModel;
            trialProgramViewModel.AdvisedSubjects = advisedSubjectsViewModel.ToList();

            SentrySdk.CaptureEvent(new SentryEvent { Message = "ViewAsPdf" });

            return new ViewAsPdf("TrialProgram", trialProgramViewModel)
            {
                FileName = "trial-program.pdf",
                PageOrientation = Rotativa.Options.Orientation.Portrait,
                PageSize = Rotativa.Options.Size.Folio,
                CustomSwitches = "--zoom 1.33"
            };
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            SentrySdk.CaptureEvent(new SentryEvent(filterContext.Exception));

            base.OnException(filterContext);
        }
    }
}