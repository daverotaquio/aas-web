﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Enums;
using AAS.Helpers;
using AAS.Models;
using Mapster;
using Microsoft.AspNet.Identity;

namespace AAS.Controllers
{
    [Authorize]
    public class EnrollmentHistoryController : BaseController
    {
        private readonly IService<Enrollment> _enrollmentService;
        private readonly IService<AcademicTerm> _academicTermService;
        private readonly IService<Adviser> _adviserService;
        private readonly IService<Student> _studentService;
        private readonly IService<StudentRecord> _studentRecordService;
        private readonly IService<CurriculumItem> _curriculumItemService;
        private readonly IService<AdvisedSubject> _advisedSubjectService;

        public EnrollmentHistoryController(
            IService<Enrollment> enrollmentService,
            IService<AcademicTerm> academicTermService,
            IService<Adviser> adviserService,
            IService<Student> studentService,
            IService<StudentRecord> studentRecordService,
            IService<CurriculumItem> curriculumItemService,
            IService<AdvisedSubject> advisedSubjectService)
        {
            _enrollmentService = enrollmentService;
            _academicTermService = academicTermService;
            _adviserService = adviserService;
            _studentService = studentService;
            _studentRecordService = studentRecordService;
            _curriculumItemService = curriculumItemService;
            _advisedSubjectService = advisedSubjectService;
        }

        public ActionResult Index()
        {
            GetCurrentUserRole();

            return View();
        }

        public PartialViewResult Create(int studentId)
        {
            var student = _studentService.GetSingle(studentId);

            //var courseId = student.CourseId.GetValueOrDefault();
            //var curriculumVersionId = student.CurriculumVersionId.GetValueOrDefault();

            //var curriculumItems = _curriculumItemService.GetAll(
            //    x => x.CourseId == courseId && x.CurriculumVersionId == curriculumVersionId,
            //    includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject, x => x.PreRequisites.Select(y => y.PreRequisite) });

            //var studentRecords = _studentRecordService.GetAll(
            //    x => x.StudentId == studentId,
            //    includeProperties: new Expression<Func<StudentRecord, object>>[] {
            //        x => x.CurriculumItem.Subject,
            //        x => x.CurriculumItem.PreRequisites,
            //        x => x.Grade,
            //        x => x.EnrollmentHistory.AcademicTerm.SchoolYear });


            //var evaluation = curriculumItems.Select(x => new StudentRecordDataTableModel
            //{
            //    Id = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.StudentRecordId,
            //    AcademicTerm = $"{x.Year.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}, {x.Semester.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}",
            //    DescriptiveTitle = x.Subject.DescriptiveTitle,
            //    FinalGrade = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Final.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name,
            //    PreRequisites = x.PreRequisites != null && x.PreRequisites.Any() ? x.PreRequisites.Select(y => y.PreRequisite.SubjectCode).Aggregate((a, b) => string.Join(", ", a, b)) : "",
            //    Status = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status.GetValueOrDefault().ToString(),
            //    SubjectCode = x.Subject.SubjectCode,
            //    CurriculumItemCourseId = studentRecords.Any(y => y.CurriculumItem.CourseId == x.CourseId) ? x.CourseId : null
            //});

            //var isShifter = studentRecords.Select(x => x.CurriculumItem.CourseId).Distinct().Count() > 1 || studentRecords.Any(x => x.CurriculumItem.CourseId != student.CourseId);
            //var isIrregular = studentRecords.Any(x => x.Grade.Status == GradeStatus.Failed);

            AcademicStanding academicStanding = AcademicStanding.Regular;

            //if (isShifter)
            //{
            //    academicStanding = AcademicStanding.Shifter;
            //}
            //else if (isIrregular)
            //{
            //    academicStanding = AcademicStanding.Irregular;
            //}

            var enrollment = new EnrollmentViewModel
            {
                StudentId = studentId,
                AcademicTerms = Enumerable.Empty<SelectListItem>(),
                Section = Section.A,
                AcademicStanding = academicStanding,
                Year = student.Year
            };

            var currentUserRole = GetCurrentUserRole();

            if (currentUserRole == "Adviser")
            {
                var currentUserId = User.Identity.GetUserId();

                var advisers = _adviserService.GetAll(x => x.UserId == currentUserId);

                var currentAdviserId = advisers.FirstOrDefault()?.Id;

                var advisersViewModel = advisers.Adapt<IEnumerable<AdviserViewModel>>();

                enrollment.Advisers = new SelectList(advisersViewModel, "Id", "FullName", currentAdviserId);
            }
            else
            {
                enrollment.Advisers = Enumerable.Empty<SelectListItem>();
            }

            return PartialView("Form", enrollment);
        }

        public PartialViewResult Edit(int id)
        {
            var enrollment = _enrollmentService.GetSingle(x => x.Id == id, x => x.AcademicTerm.SchoolYear);

            var enrollmentViewModel = enrollment.Adapt<EnrollmentViewModel>();

            var academicTerms = _academicTermService.GetAll(includeProperties: x => x.SchoolYear);

            var academicTermsViewModel = academicTerms.Adapt<IEnumerable<AcademicTermViewModel>>();

            var advisers = _adviserService.GetAll();

            var advisersViewModel = advisers.Adapt<IEnumerable<AdviserViewModel>>();

            var studentId = enrollment.StudentId.GetValueOrDefault();

            var student = _studentService.GetSingle(studentId);

            enrollmentViewModel.Year = student.Year;
            enrollmentViewModel.Advisers = new SelectList(advisersViewModel, "Id", "FullName", enrollmentViewModel.AdviserId);
            enrollmentViewModel.AcademicTerms = new SelectList(academicTermsViewModel, "Id", "FullName", enrollmentViewModel.AcademicTermId);

            return PartialView("Form", enrollmentViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var enrollment = _enrollmentService.GetSingle(id);

            var enrollmentViewModel = enrollment.Adapt<EnrollmentViewModel>();

            return PartialView("Delete", enrollmentViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(EnrollmentViewModel model)
        {
            var result = _enrollmentService.Delete(model.Id);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _enrollmentService.GetSingle(id);

            var enrollmentViewModel = result.Adapt<EnrollmentViewModel>();

            return PartialView(enrollmentViewModel);
        }

        public JsonResult Save(EnrollmentViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var currentEnrollment = _enrollmentService.GetSingle(model.Id);

                var updatedEnrollment = model.Adapt(currentEnrollment);

                var enrollmentResult = _enrollmentService.Update(updatedEnrollment);

                var studentId = updatedEnrollment.StudentId.GetValueOrDefault();

                var student = _studentService.GetSingle(studentId);

                student.Year = model.Year;

                var studentResult = _studentService.Update(student);

                result = enrollmentResult && studentResult;
            }
            else
            {
                if (model.AcademicTermId > 0)
                {
                    var enrollment = model.Adapt<Enrollment>();

                    result = _enrollmentService.Add(enrollment);

                    var academicTermId = enrollment.AcademicTermId.GetValueOrDefault();

                    var academicTerm = _academicTermService.GetSingle(academicTermId);

                    if (enrollment.Year == Year.First && academicTerm.Semester == Semester.FirstSemster && enrollment.AcademicStanding == AcademicStanding.Regular)
                    {
                        if (result)
                        {
                            var enrollmentId = enrollment.Id;
                            var studentId = enrollment.StudentId.GetValueOrDefault();
                            var student = _studentService.GetSingle(studentId);
                            var courseId = student.CourseId;
                            var curriculumVersionId = student.CurriculumVersionId;
                            var preferredSection = enrollment.Section;

                            var curriculumItems = _curriculumItemService.GetAll(x => x.Year == Year.First && x.Semester == Semester.FirstSemster && x.CourseId == courseId && x.CurriculumVersionId == curriculumVersionId);

                            var advisedSubjects = curriculumItems.Select(x => new AdvisedSubject
                            {
                                CurriculumItemId = x.Id,
                                EnrollmentId = enrollmentId,
                                StudentId = studentId,
                                PreferredSection = preferredSection
                            });

                            result = result && _advisedSubjectService.AddRange(advisedSubjects);
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var result = _enrollmentService.GetAll().OrderByDescending(x => x.Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AcademicTerms(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _academicTermService.GetAll(includeProperties: new Expression<Func<AcademicTerm, object>>[] { x => x.SchoolYear }) : _academicTermService.GetAll(x => x.SchoolYear.Description.Contains(q) || x.Semester.ToString().Contains(q), includeProperties: new Expression<Func<AcademicTerm, object>>[] { x => x.SchoolYear });

            var academicTerms = result.Adapt<IEnumerable<AcademicTermViewModel>>();

            return Json(academicTerms.OrderByDescending(x => x.SchoolYear.Description).ThenBy(x => x.Semester), JsonRequestBehavior.AllowGet);
        }

        public JsonResult Advisers(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _adviserService.GetAll() : _adviserService.GetAll(x => x.FirstName.Contains(q) || x.MiddleName.Contains(q) || x.LastName.Contains(q));

            var advisers = result.Adapt<IEnumerable<AdviserViewModel>>();

            return Json(advisers, JsonRequestBehavior.AllowGet);
        }
    }
}