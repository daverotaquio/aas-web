﻿using AAS.App_Start;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.Persistence.Interface;
using Microsoft.AspNet.Identity;
using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAS.Controllers
{
    public class HomeController : BaseController
    {
        [Authorize]
        public ActionResult Index()
        {
            var currentUserRole = GetCurrentUserRole();

            if (currentUserRole == "Student")
            {
                var studentService = NinjectWebCommon.bootstrapper.Kernel.Get<IService<Student>>();

                var currentUserId = User.Identity.GetUserId();

                var student = studentService.GetSingle(x => x.UserId == currentUserId);

                return RedirectToAction("advisement", "students", new { id = student.Id });
            }

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}