﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Enums;
using AAS.Helpers;
using AAS.Models;
using Mapster;

namespace AAS.Controllers
{
    [Authorize]
    public class StudentRecordsController : Controller
    {
        private readonly IService<StudentRecord> _studentRecordService;
        private readonly IService<CurriculumItem> _curriculumItemService;
        private readonly IService<AcademicTerm> _academicTermService;
        private readonly IService<Enrollment> _enrollmentService;
        private readonly IService<Student> _studentService;
        private readonly IService<AdvisedSubject> _advisedSubjectService;

        public StudentRecordsController(
            IService<StudentRecord> studentRecordService,
            IService<CurriculumItem> curriculumItemService,
            IService<AcademicTerm> academicTermService,
            IService<Enrollment> enrollmentService,
            IService<Student> studentService,
            IService<AdvisedSubject> advisedSubjectService
            )
        {
            _studentRecordService = studentRecordService;
            _curriculumItemService = curriculumItemService;
            _academicTermService = academicTermService;
            _enrollmentService = enrollmentService;
            _studentService = studentService;
            _advisedSubjectService = advisedSubjectService;
        }

        public PartialViewResult Create(int studentId)
        {
            var studentRecordViewModel = new StudentRecordViewModel
            {
                StudentId = studentId,
                CurriculumItems = Enumerable.Empty<SelectListItem>(),
                EnrolledAcademicTerms = Enumerable.Empty<SelectListItem>(),
                AcademicTerms = Enumerable.Empty<SelectListItem>(),
                IsCreate = true,
                Grade = new GradeViewModel
                {
                    GradesSelection = DropdownHelper.Grades
                }
            };

            return PartialView("Form", studentRecordViewModel);
        }

        public PartialViewResult Edit(int id, string d)
        {
            var studentRecord = _studentRecordService.GetSingle(id);

            var studentRecordViewModel = studentRecord.Adapt<StudentRecordViewModel>();

            var curriculumItems = _curriculumItemService.GetAll(includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject });
            var enrollment = _enrollmentService.GetAll(includeProperties: new Expression<Func<Enrollment, object>>[] { x => x.AcademicTerm.SchoolYear });
            var academicTerms = _academicTermService.GetAll(includeProperties: new Expression<Func<AcademicTerm, object>>[] { x => x.SchoolYear });

            var curriculumItemsViewModel = curriculumItems.Adapt<IEnumerable<CurriculumItemViewModel>>();
            var enrollmentsViewModel = enrollment.Adapt<IEnumerable<EnrollmentViewModel>>();
            var academicTermsViewModel = enrollment.Adapt<IEnumerable<AcademicTermViewModel>>();

            studentRecordViewModel.CurriculumItems = new SelectList(curriculumItemsViewModel, "Id", "FullName", studentRecordViewModel.CurriculumItemId);
            studentRecordViewModel.EnrolledAcademicTerms = new SelectList(enrollmentsViewModel, "Id", "FullName", studentRecordViewModel.EnrollmentId);
            studentRecordViewModel.AcademicTerms = new SelectList(academicTermsViewModel, "Id", "FullName", studentRecordViewModel.AcademicTermId);

            if (string.IsNullOrEmpty(d))
            {
                studentRecordViewModel.IsCreate = false;

                return PartialView("Form", studentRecordViewModel);
            }
            else
            {
                return PartialView("Edit", studentRecordViewModel);
            }
        }

        public JsonResult Save(StudentRecordViewModel model)
        {
            bool result;

            var enrollemntId = model.EnrollmentId.GetValueOrDefault();

            var enrollment = _enrollmentService.GetSingle(enrollemntId);

            if (model.Id > 0)
            {
                var currentStudentRecord = _studentRecordService.GetSingle(model.Id);

                var updatedStudentRecord = model.Adapt(currentStudentRecord);
                // please recheck
                updatedStudentRecord.AcademicTermId = enrollment.AcademicTermId;

                result = _studentRecordService.Update(updatedStudentRecord);
            }
            else
            {
                var studentRecord = model.Adapt<StudentRecord>();

                studentRecord.AcademicTermId = enrollment?.AcademicTermId ?? model.AcademicTermId;

                var studentRecordResult = _studentRecordService.Add(studentRecord);

                result = studentRecordResult;
            }

            //var activeEnrollment = _enrollmentService.GetSingle(x => x.StudentId == model.StudentId && x.EnrollmentStatus == EnrollmentStatus.Active && x.AdmissionStatus == AdmissionStatus.AdvisementCompleted);

            //var activeEnrollmentId = activeEnrollment?.Id;

            //if (activeEnrollmentId != null)
            //{
            //    var outstandingStudentRecords = _studentRecordService.GetAll(
            //    x => (!x.Grade.Final.HasValue || x.Grade.Status == GradeStatus.Unposted) && x.StudentId == model.StudentId && x.EnrollmentId == activeEnrollmentId);

            //    if (outstandingStudentRecords.Count() == 0)
            //    {
            //        activeEnrollment.EnrollmentStatus = EnrollmentStatus.Completed;

            //        var updatedEnrollmentResult = _enrollmentService.Update(activeEnrollment);
            //    }
            //}

            return Json(new { success = result });
        }

        public JsonResult UpdateEnrollment(int id)
        {
            bool result = false;

            var activeEnrollment = _enrollmentService.GetSingle(x => x.StudentId == id && x.EnrollmentStatus == EnrollmentStatus.Active && x.AdmissionStatus == AdmissionStatus.AdvisementCompleted);

            var activeEnrollmentId = activeEnrollment?.Id;

            if (activeEnrollmentId != null)
            {
                var outstandingStudentRecords = _studentRecordService.GetAll(
                x => (!x.Grade.Final.HasValue || x.Grade.Status == GradeStatus.Unposted) && x.StudentId == id && x.EnrollmentId == activeEnrollmentId);

                if (outstandingStudentRecords.Count() == 0)
                {
                    activeEnrollment.EnrollmentStatus = EnrollmentStatus.Completed;

                    result = _enrollmentService.Update(activeEnrollment);
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult List(int id)
        {
            var student = _studentService.GetSingle(id);

            var courseId = student.CourseId.GetValueOrDefault();
            var curriculumVersionId = student.CurriculumVersionId.GetValueOrDefault();

            var curriculumItems = _curriculumItemService.GetAll(
                x => x.CourseId == courseId && x.CurriculumVersionId == curriculumVersionId,
                includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject, x => x.PreRequisites.Select(y => y.PreRequisite) });

            var studentRecords = _studentRecordService.GetAll(
                x => x.StudentId == id,
                includeProperties: new Expression<Func<StudentRecord, object>>[] {
                    x => x.CurriculumItem.Subject,
                    x => x.CurriculumItem.PreRequisites,
                    x => x.Grade,
                    x => x.EnrollmentHistory.AcademicTerm.SchoolYear });

            var evaluation = curriculumItems.Select(x => new StudentRecordDataTableModel
            {
                Id = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.StudentRecordId,
                AcademicTerm = $"{x.Year.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}, {x.Semester.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}",
                DescriptiveTitle = x.Subject.DescriptiveTitle,
                FinalGrade = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Final.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name,
                PreRequisites = x.PreRequisites != null && x.PreRequisites.Any() ? x.PreRequisites.Select(y => y.PreRequisite.SubjectCode).Aggregate((a, b) => string.Join(", ", a, b)) : "",
                Status = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status.GetValueOrDefault().ToString(),
                SubjectCode = x.Subject.SubjectCode,
                CurriculumItemId = x.Id
            });

            return Json(evaluation, JsonRequestBehavior.AllowGet);
        }
        public JsonResult Prospectus(int id)
        {
            var student = _studentService.GetSingle(x => x.Id == id, includeProperties: new Expression<Func<Student, object>>[] { x => x.Enrollments.Select(y => y.AcademicTerm.SchoolYear) });

            var courseId = student.CourseId.GetValueOrDefault();
            var curriculumVersionId = student.CurriculumVersionId.GetValueOrDefault();

            var curriculumItems = _curriculumItemService.GetAll(
                x => x.CourseId == courseId && x.CurriculumVersionId == curriculumVersionId,
                includeProperties: new Expression<Func<CurriculumItem, object>>[] {
                    x => x.Subject, x => x.PreRequisites.Select(y => y.PreRequisite) });

            var studentRecords = _studentRecordService.GetAll(
                x => x.StudentId == id,
                includeProperties: new Expression<Func<StudentRecord, object>>[] {
                    x => x.CurriculumItem.Subject,
                    x => x.CurriculumItem.PreRequisites,
                    x => x.Grade,
                    x => x.EnrollmentHistory.AcademicTerm.SchoolYear });

            var advisedSubjects = _advisedSubjectService.GetAll(
                x => x.StudentId == id,
                includeProperties: new Expression<Func<AdvisedSubject, object>>[] {
                    x => x.CurriculumItem.Subject,
                    x => x.CurriculumItem.PreRequisites.Select(y => y.PreRequisite) });

            var evaluation = curriculumItems.Select(x => new StudentRecordDataTableModel
            {
                Id = x.Id,
                AcademicTerm = $"{x.Year.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}, {x.Semester.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}",
                DescriptiveTitle = x.Subject.DescriptiveTitle,
                FinalGrade = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Final.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name,
                PreRequisites = x.PreRequisites != null && x.PreRequisites.Any() ? x.PreRequisites.Select(y => { return $"<span>{y.PreRequisite.SubjectCode}</span>"; }).Aggregate((a, b) => string.Join("<br />", a, b)) : "",
                Status = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status.GetValueOrDefault() == GradeStatus.Passed
                ? studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status.GetValueOrDefault().ToString()
                : x.PreRequisites != null && x.PreRequisites.Any()
                ? x.PreRequisites.Where(y => studentRecords.LastOrDefault(z => z.CurriculumItem.SubjectId == y.PreRequisiteId)?.Grade?.Status != GradeStatus.Passed).DefaultIfEmpty().Select(y => { return y != null
                    ? $"{y.PreRequisite.SubjectCode} - {studentRecords.LastOrDefault(z => z.CurriculumItem.SubjectId == y.PreRequisiteId)?.Grade?.Status?.ToString() ?? "Not Passed"}"
                    : ""; }).Aggregate((a, b) => string.Join("<br />", a, b))
                : studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status.GetValueOrDefault().ToString(),
                SubjectCode = x.Subject.SubjectCode,
                Units = x.Subject.Units.ToString(),
                IsAvailable = (studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status != GradeStatus.Passed) && ((x.PreRequisites == null || !x.PreRequisites.Any()) && !studentRecords.Any(y => y.CurriculumItem.SubjectId == x.SubjectId && y.Grade.Status == GradeStatus.Passed) || x.PreRequisites.Any() && x.PreRequisites.All(y => studentRecords.LastOrDefault(z => z.CurriculumItem.SubjectId == y.PreRequisiteId)?.Grade?.Status == GradeStatus.Passed)),
                IsAdvised = advisedSubjects.Any(y => y.CurriculumItem.SubjectId == x.SubjectId),
                IsSemesterMatching = x.Semester == student.Enrollments.FirstOrDefault(y => y.EnrollmentStatus == EnrollmentStatus.Active)?.AcademicTerm?.Semester,
                SemesterMatchMessage = x.Semester != student.Enrollments.FirstOrDefault(y => y.EnrollmentStatus == EnrollmentStatus.Active)?.AcademicTerm?.Semester ? "Subject may not be available this semester" : "",
                IsYearMatching = x.Year == student.Enrollments.FirstOrDefault(y => y.EnrollmentStatus == EnrollmentStatus.Active)?.Year,
                YearMatchMessage = x.Year != student.Enrollments.FirstOrDefault(y => y.EnrollmentStatus == EnrollmentStatus.Active)?.Year ? $"This subject is for {x.Year.GetAttribute<DisplayAttribute>().Name} students" : ""
            });

            return Json(evaluation, JsonRequestBehavior.AllowGet);
        }

        public JsonResult EnrollmentHistory(int? id)
        {
            var result = _enrollmentService.GetAll(x => x.StudentId == id, includeProperties: x => x.AcademicTerm.SchoolYear);

            var dataTableModel = result.Adapt<IEnumerable<EnrollmentViewModel>>().Select(x => new EnrollmentHistoryDataTableModel
            {
                Id = x.Id.ToString(),
                AcademicTerm = x.AcademicTerm?.FullName,
                Status = x.AdmissionStatus.GetAttribute<DisplayAttribute>()?.Name,
                StudentId = x.StudentId.ToString(),
                AcademicStanding = x.AcademicStanding.GetValueOrDefault().ToString(),
                Year = x.Year.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name,
                EnrollmentStatus = x.EnrollmentStatus.ToString()
            });

            return Json(dataTableModel, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CurriculumItems(int id, string q)
        {
            var student = _studentService.GetSingle(x => x.Id == id, includeProperties: new Expression<Func<Student, object>>[] { x => x.Enrollments.Select(y => y.AcademicTerm.SchoolYear) });

            var courseId = student.CourseId.GetValueOrDefault();

            var result = string.IsNullOrEmpty(q) ? _curriculumItemService.GetAll(x => x.CourseId == courseId, includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject }) : _curriculumItemService.GetAll(x => x.CourseId == courseId && x.Subject.SubjectCode.Contains(q) || x.Subject.DescriptiveTitle.Contains(q), includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject });

            var curriculumItems = result.Adapt<IEnumerable<CurriculumItemViewModel>>();

            return Json(curriculumItems, JsonRequestBehavior.AllowGet);
        }

        public JsonResult AcademicTerms(string q)
        {
            var result = string.IsNullOrEmpty(q) ? _academicTermService.GetAll(includeProperties: new Expression<Func<AcademicTerm, object>>[] { x => x.SchoolYear }) : _academicTermService.GetAll(x => x.SchoolYear.Description.Contains(q) || x.Semester.ToString().Contains(q), includeProperties: new Expression<Func<AcademicTerm, object>>[] { x => x.SchoolYear });

            var academicTerms = result.Adapt<IEnumerable<AcademicTermViewModel>>();

            return Json(academicTerms.OrderByDescending(x => x.SchoolYear.Description).ThenBy(x => x.Semester), JsonRequestBehavior.AllowGet);
        }

        public JsonResult FinalizeAdvisement(List<StudentRecordViewModel> model)
        {
            var studentId = model.FirstOrDefault().StudentId;

            var student = _studentService.GetSingle(x => x.Id == studentId, includeProperties: x => x.Enrollments);

            var activeEnrollment = student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active)?.Adapt<EnrollmentViewModel>();

            var activeEnrollmentId = activeEnrollment?.Id;

            var result = false;

            if (activeEnrollmentId != null)
            {
                var newStudentRecords = model.Adapt<IEnumerable<StudentRecord>>();

                newStudentRecords = newStudentRecords.Select(x => { x.EnrollmentId = activeEnrollmentId; x.AcademicTermId = activeEnrollment.AcademicTermId; return x; });

                var studentRecordsResult = _studentRecordService.AddRange(newStudentRecords);

                activeEnrollment.AdmissionStatus = AdmissionStatus.AdvisementCompleted;

                //activeEnrollment.Student = null;
                var id = activeEnrollmentId.GetValueOrDefault();

                var enrollment = _enrollmentService.GetSingle(id);

                enrollment.AdmissionStatus = AdmissionStatus.AdvisementCompleted;

                var updatedEnrollmentResult = _enrollmentService.Update(enrollment);

                var deleteAdvisedSubjectsResult = _advisedSubjectService.DeleteRange(x => x.StudentId == studentId);

                result = studentRecordsResult && updatedEnrollmentResult && deleteAdvisedSubjectsResult;
            }

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }
    }
}