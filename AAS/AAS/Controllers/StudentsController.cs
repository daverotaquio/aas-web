﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Enums;
using AAS.Helpers;
using AAS.Models;
using Mapster;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using IndexPageViewModel = AAS.Models.IndexPageViewModel;

namespace AAS.Controllers
{
    [Authorize]
    public class StudentsController : BaseController
    {
        private readonly IService<Student> _studentService;
        private readonly IService<Course> _courseService;
        private readonly IService<StudentRecord> _studentRecordService;
        private readonly IService<CurriculumVersion> _curriculumVersionService;
        private readonly IService<CurriculumItem> _curriculumItemService;
        private readonly IService<Enrollment> _enrollmentService;
        private readonly IService<Adviser> _adviserService;

        public StudentsController(
            IService<Student> studentService,
            IService<Course> courseService,
            IService<StudentRecord> studentRecordService,
            IService<CurriculumVersion> curriculumVersionService,
            IService<CurriculumItem> curriculumItemService,
            IService<Enrollment> enrollmentService,
            IService<Adviser> adviserService
            )
        {
            _studentService = studentService;
            _courseService = courseService;
            _studentRecordService = studentRecordService;
            _curriculumVersionService = curriculumVersionService;
            _curriculumItemService = curriculumItemService;
            _enrollmentService = enrollmentService;
            _adviserService = adviserService;
        }

        public ActionResult Index()
        {
            var currentUserRole = GetCurrentUserRole();

            if (currentUserRole == "Registrar")
            {
                ViewBag.IsUnauthorized = true;
                ViewBag.IsUnauthorizedCreate = true;
                ViewBag.IsUnauthorizedEdit = true;
                ViewBag.IsUnauthorizedDelete = true;
            }

            return View();
        }

        public PartialViewResult Create()
        {
            var studentViewModel = new StudentViewModel ();

            var currentUserId = User.Identity.GetUserId();
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var isAdviser = userManager.IsInRole(currentUserId, "Adviser");

            Adviser currentAdviser = null;

            if (isAdviser)
            {
                currentAdviser = _adviserService.GetSingle(x => x.UserId == currentUserId);
            }

            studentViewModel.Courses = isAdviser ? new SelectList(_courseService.GetAll(x => x.Id == currentAdviser.CourseId), "Id", "Name", currentAdviser.CourseId) : new SelectList(_courseService.GetAll(), "Id", "Name");
            studentViewModel.CurriculumVersions = isAdviser ? new SelectList(_curriculumVersionService.GetAll(x => x.CourseId == currentAdviser.CourseId), "Id", "ShortDescription") : new SelectList(_curriculumVersionService.GetAll(), "Id", "ShortDescription");

            return PartialView("Form", studentViewModel);
        }

        public PartialViewResult Edit(int id)
        {
            var student = _studentService.GetSingle(x => x.Id == id, x => x.User);

            var studentViewModel = student.Adapt<StudentViewModel>();

            var currentUserId = User.Identity.GetUserId();
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var isAdviser = userManager.IsInRole(currentUserId, "Adviser");

            Adviser currentAdviser = null;

            if (isAdviser)
            {
                currentAdviser = _adviserService.GetSingle(x => x.UserId == currentUserId);
            }

            studentViewModel.Courses = isAdviser ? new SelectList(_courseService.GetAll(x => x.Id == currentAdviser.CourseId), "Id", "Name", currentAdviser.CourseId) : new SelectList(_courseService.GetAll(), "Id", "Name", studentViewModel.CourseId);
            studentViewModel.CurriculumVersions = isAdviser ? new SelectList(_curriculumVersionService.GetAll(x => x.CourseId == currentAdviser.CourseId), "Id", "ShortDescription") : new SelectList(_curriculumVersionService.GetAll(), "Id", "ShortDescription", studentViewModel.CurriculumVersionId);

            return PartialView("Form", studentViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var student = _studentService.GetSingle(id);

            var studentViewModel = student.Adapt<StudentViewModel>();

            return PartialView("Delete", studentViewModel);
        }

        [HttpPost]
        public async Task<JsonResult> ConfirmDelete(StudentViewModel model)
        {
            var result = _studentService.Delete(model.Id);

            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

            var currentUser = await userManager.FindByIdAsync(model.UserId);

            var deleteResult = await userManager.DeleteAsync(currentUser);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _studentService.GetSingle(id);

            var studentViewModel = result.Adapt<StudentViewModel>();

            return PartialView(studentViewModel);
        }

        public async Task<JsonResult> Save(StudentViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var currentUser = await userManager.FindByIdAsync(model.UserId);

                currentUser.Email = model.User.Email;

                var userResult = await userManager.UpdateAsync(currentUser);

                if (userResult.Succeeded)
                {
                    var currentStudent = _studentService.GetSingle(x => x.Id == model.Id);

                    var updatedStudent = model.Adapt(currentStudent);
                    updatedStudent.User = null;

                    result = _studentService.Update(updatedStudent);

                    var studentId = updatedStudent.Id;

                    var lastEnrollmentRecord = _enrollmentService.GetAll(x => x.StudentId == studentId).LastOrDefault();

                    if (lastEnrollmentRecord != null)
                    {
                        if (lastEnrollmentRecord.EnrollmentStatus == EnrollmentStatus.Active)
                        {
                            lastEnrollmentRecord.Year = updatedStudent.Year;

                            var enrollmentResult = _enrollmentService.Update(lastEnrollmentRecord);
                        }
                    }
                }
                else
                {
                    result = false;
                }
            }
            else
            {
                var currentUserId = User.Identity.GetUserId();
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var isAdviser = userManager.IsInRole(currentUserId, "Adviser");

                Adviser currentAdviser = null;

                if (isAdviser)
                {
                    currentAdviser = _adviserService.GetSingle(x => x.UserId == currentUserId);
                    model.CourseId = currentAdviser.CourseId;
                }

                var user = new ApplicationUser { UserName = model.User.Email, Email = model.User.Email };

                var userManagerResult = await userManager.CreateAsync(user, "12345678");

                if (userManagerResult.Succeeded)
                {
                    userManager.AddToRole(user.Id, "Student");

                    model.UserId = user.Id;

                    var student = model.Adapt<Student>();
                    student.User = null;

                    result = _studentService.Add(student);
                }
                else
                {
                    result = false;
                }
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var currentUserId = User.Identity.GetUserId();
            var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var isAdviser = userManager.IsInRole(currentUserId, "Adviser");

            Adviser currentAdviser = null;

            if (isAdviser)
            {
                currentAdviser = _adviserService.GetSingle(x => x.UserId == currentUserId);
            }

            var result = isAdviser ?
                _studentService.GetAll(x => x.CourseId == currentAdviser.CourseId, includeProperties: new Expression<Func<Student, object>>[] { x => x.User, x => x.Course, x => x.CurriculumVersion, x => x.Enrollments }).OrderByDescending(x => x.Id) :
                _studentService.GetAll(includeProperties: new Expression<Func<Student, object>>[] { x => x.User, x => x.Course, x => x.CurriculumVersion, x => x.Enrollments }).OrderByDescending(x => x.Id);

            var students = result.Adapt<IEnumerable<StudentViewModel>>();

            var studentsDataTableModel = students.Select(x => new StudentDataTableModel
            {
                AcademicStanding = x.Enrollments.LastOrDefault()?.AcademicStanding.ToString(),
                ContactNumber = x.ContactNumber,
                Course = x.Course.Name,
                Email = x.User.Email,
                FullName = x.FullName,
                Gender = x.GenderString,
                Id = x.Id,
                IdNumber = x.IDNumber,
                Year = x.Year?.GetAttribute<DisplayAttribute>()?.Name
            });

            return Json(studentsDataTableModel, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Advisement(int id)
        {
            var advisementViewModel = new AdvisementViewModel();

            var student = _studentService.GetSingle(x => x.Id == id, new Expression<Func<Student, object>>[] { x => x.User, y => y.Course, z => z.Enrollments.Select(x => x.AcademicTerm.SchoolYear), x => x.Enrollments.Select(y => y.Adviser) });
            //var studentRecords = _studentRecordService.GetAll(includeProperties: new Expression<Func<StudentRecord, object>>[] { x => x.CurriculumItem.Subject, x => x.CurriculumItem.PreRequisites, y => y.Grade, z => z.EnrollmentHistory.AcademicTerm.SchoolYear });

            var activeEnrollment = student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active);

            var activeEnrollmentId = activeEnrollment?.Id;

            var outstandingstudentRecords = _studentRecordService.GetAll(x => (!x.Grade.Final.HasValue && x.Grade.Status == GradeStatus.Unposted && x.StudentId == id && x.EnrollmentId == activeEnrollmentId), includeProperties: new Expression<Func<StudentRecord, object>>[] { x => x.CurriculumItem, y => y.Grade, z => z.EnrollmentHistory.AcademicTerm.SchoolYear });

            var outstandingStudentRecordsViewModel = outstandingstudentRecords.Adapt<IEnumerable<StudentRecordViewModel>>();

            var studentViewModel = student.Adapt<StudentViewModel>();
            //var studentRecordsViewModel = studentRecords.Adapt<IEnumerable<StudentRecordViewModel>>();

            advisementViewModel.Student = studentViewModel;
            //advisementViewModel.StudentRecords = studentRecordsViewModel;
            advisementViewModel.OutstandingStudentRecords = outstandingStudentRecordsViewModel;

            advisementViewModel.AdvisedSubjectInfo = AdvisementHelper.GetAllowedUnits(student.Id);

            var currentUserRole = GetCurrentUserRole();

            var indexPageViewModel = new IndexPageViewModel
            {
                CurrentUserRole = currentUserRole
            };

            advisementViewModel.IndexPageViewModel = indexPageViewModel;

            return View(advisementViewModel);
        }

        public JsonResult StudentRecords()
        {
            var studentRecords = _studentRecordService.GetAll(includeProperties: new Expression<Func<StudentRecord, object>>[] { x => x.CurriculumItem, y => y.Grade, z => z.EnrollmentHistory.AcademicTerm.SchoolYear });

            return Json(studentRecords, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult AdvisementTab(int id)
        {
            var student = _studentService.GetSingle(x => x.Id == id, includeProperties: new Expression<Func<Student, object>>[] { x => x.Enrollments.Select(y => y.AcademicTerm.SchoolYear), x => x.Enrollments.Select(y => y.Adviser) });

            var currentEnrollment = student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active);

            var currentEnrollmentViewModel = currentEnrollment.Adapt<EnrollmentViewModel>();
            
            if (currentEnrollmentViewModel != null)
            {
                currentEnrollmentViewModel.AdvisedSubjectInfo = AdvisementHelper.GetAllowedUnits(student.Id);
            }

            return PartialView(currentEnrollmentViewModel);
        }

        public PartialViewResult AdvisementTabDetails(int id)
        {
            var student = _studentService.GetSingle(x => x.Id == id, includeProperties: new Expression<Func<Student, object>>[] { x => x.Enrollments.Select(y => y.AcademicTerm.SchoolYear), x => x.Enrollments.Select(y => y.Adviser) });

            var currentEnrollment = student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active);

            var currentEnrollmentViewModel = currentEnrollment.Adapt<EnrollmentViewModel>();

            if (currentEnrollmentViewModel != null)
            {
                currentEnrollmentViewModel.AdvisedSubjectInfo = AdvisementHelper.GetAllowedUnits(student.Id);
            }

            return PartialView(currentEnrollmentViewModel);
        }

        public PartialViewResult OutstandingSubjectsTab(int id)
        {
            var student = _studentService.GetSingle(x => x.Id == id, new Expression<Func<Student, object>>[] { x => x.Enrollments });

            var activeEnrollment = student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active)?.Adapt<EnrollmentViewModel>();

            var activeEnrollmentId = activeEnrollment?.Id;

            var studentRecords = _studentRecordService.GetAll(x => (!x.Grade.Final.HasValue && x.Grade.Status == GradeStatus.Unposted && x.StudentId == id && x.EnrollmentId == activeEnrollmentId), includeProperties: new Expression<Func<StudentRecord, object>>[] { x => x.CurriculumItem, y => y.Grade, z => z.EnrollmentHistory.AcademicTerm.SchoolYear });

            var studentRecordsViewModel = studentRecords.Adapt<IEnumerable<StudentRecordViewModel>>();

            return PartialView(studentRecordsViewModel);
        }

        public JsonResult OutstandingSubjects(int id)
        {
            var student = _studentService.GetSingle(x => x.Id == id, new Expression<Func<Student, object>>[] { x => x.Enrollments });

            var activeEnrollment = student.Enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active)?.Adapt<EnrollmentViewModel>();

            var activeEnrollmentId = activeEnrollment?.Id;

            var courseId = student.CourseId.GetValueOrDefault();
            var curriculumVersionId = student.CurriculumVersionId.GetValueOrDefault();

            var curriculumItems = _curriculumItemService.GetAll(
                x => x.CourseId == courseId && x.CurriculumVersionId == curriculumVersionId,
                includeProperties: new Expression<Func<CurriculumItem, object>>[] { x => x.Subject, x => x.PreRequisites.Select(y => y.PreRequisite) });

            var studentRecords = _studentRecordService.GetAll(
                x => (!x.Grade.Final.HasValue || x.Grade.Status == GradeStatus.Unposted) && x.StudentId == id && x.EnrollmentId == activeEnrollmentId,
                includeProperties: new Expression<Func<StudentRecord, object>>[] {
                    x => x.CurriculumItem.Subject,
                    x => x.CurriculumItem.PreRequisites,
                    x => x.Grade,
                    x => x.EnrollmentHistory.AcademicTerm.SchoolYear });

            var outstandingSubjects = curriculumItems.Select(x => new StudentRecordDataTableModel
            {
                Id = studentRecords.LastOrDefault(y => y.CurriculumItemId == x.Id)?.StudentRecordId,
                AcademicTerm = $"{x.Year.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}, {x.Semester.GetValueOrDefault().GetAttribute<DisplayAttribute>().Name}",
                DescriptiveTitle = x.Subject.DescriptiveTitle,
                FinalGrade = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Final.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name,
                PreRequisites = x.PreRequisites != null && x.PreRequisites.Any() ? x.PreRequisites.Select(y => y.PreRequisite.SubjectCode).Aggregate((a, b) => string.Join(", ", a, b)) : "",
                Status = studentRecords.LastOrDefault(y => y.CurriculumItem.SubjectId == x.SubjectId)?.Grade?.Status.GetValueOrDefault().ToString(),
                SubjectCode = x.Subject.SubjectCode
            }).Where(x => !string.IsNullOrEmpty(x.Status));

            return Json(outstandingSubjects, JsonRequestBehavior.AllowGet);
        }
    }
}