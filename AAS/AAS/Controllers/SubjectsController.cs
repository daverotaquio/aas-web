﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.Models;
using Mapster;

namespace AAS.Controllers
{
    [Authorize]
    public class SubjectsController : BaseController
    {
        private readonly IService<Subject> _subjectService;
        private readonly IService<Adviser> _adviserService;
        private readonly IService<CurriculumItemPreRequisites> _subjectSpecificationService;

        public SubjectsController(
            IService<Subject> service,
            IService<Adviser> adviserService,
            IService<CurriculumItemPreRequisites> subjectSpecificationService)
        {
            _subjectService = service;
            _adviserService = adviserService;
            _subjectSpecificationService = subjectSpecificationService;
        }

        public ActionResult Index()
        {
            var currentUserRole = GetCurrentUserRole();

            if (currentUserRole != "Registrar")
            {
                ViewBag.IsUnauthorized = true;
                ViewBag.IsUnauthorizedCreate = true;
                ViewBag.IsUnauthorizedEdit = true;
            }

            return View();
        }

        public PartialViewResult Create()
        {
            var subjectViewModel = new SubjectViewModel();

            return PartialView("Form", subjectViewModel);
        }

        public PartialViewResult Edit(int id)
        {
            var subject = _subjectService.GetSingle(id);

            var subjectViewModel = subject.Adapt<SubjectViewModel>();

            return PartialView("Form", subjectViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var subject = _subjectService.GetSingle(id);

            var subjectViewModel = subject.Adapt<SubjectViewModel>();

            return PartialView("Delete", subjectViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(SubjectViewModel model)
        {
            var subjectResult = _subjectService.Delete(model.Id);

            var subjectSpecificationResult = _subjectSpecificationService.DeleteRange(x => x.CurriculumItemtId == model.Id);

            var result = subjectResult && subjectSpecificationResult;

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _subjectService.GetSingle(id);

            var subjectViewModel = result.Adapt<SubjectViewModel>();

            return PartialView(subjectViewModel);
        }

        public JsonResult Save(SubjectViewModel model)
        {
            bool result;

            if (model.Id > 0)
            {
                var currentSubject = _subjectService.GetSingle(model.Id);

                var updatedSubject = model.Adapt(currentSubject);

                result = _subjectService.Update(updatedSubject);
            }
            else
            {
                var subject = model.Adapt<Subject>();

                result = _subjectService.Add(subject);
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var result = _subjectService.GetAll().OrderByDescending(x => x.Id);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}