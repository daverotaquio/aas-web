﻿using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.DataTableModels;
using AAS.Models;
using Mapster;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace AAS.Controllers
{
    [Authorize]
    public class UsersController : BaseController
    {
        private readonly IService<Adviser> _adviserService;

        public UsersController(IService<Adviser> adviserService)
        {
            _adviserService = adviserService;
        }

        public ActionResult Index()
        {
            GetCurrentUserRole();

            return View();
        }

        public PartialViewResult Create()
        {
            var adviserViewModel = new AdviserViewModel();

            var context = _adviserService.GetContext();

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var roleToRemove = roleMngr.Roles.FirstOrDefault(x => x.Name == "Student");

            var roles = roleMngr.Roles.ToList();
            roles.Remove(roleToRemove);

            adviserViewModel.Roles = new SelectList(roles, "Id", "Name");

            return PartialView("Form", adviserViewModel);
        }

        public PartialViewResult Edit(int id)
        {
            var adviser = _adviserService.GetSingle(x => x.Id == id, includeProperties: x => x.User);

            var context = _adviserService.GetContext();

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var roleToRemove = roleMngr.Roles.FirstOrDefault(x => x.Name == "Student");

            var roles = roleMngr.Roles.ToList();
            roles.Remove(roleToRemove);

            var adviserViewModel = adviser.Adapt<AdviserViewModel>();
            
            var currentRole = adviser.User.Roles.Any() ? adviser.User.Roles.FirstOrDefault() : null;
            var currentRoleId = currentRole.RoleId;

            adviserViewModel.Roles = new SelectList(roles, "Id", "Name", currentRoleId);

            return PartialView("Form", adviserViewModel);
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            var adviser = _adviserService.GetSingle(id);

            var adviserViewModel = adviser.Adapt<AdviserViewModel>();

            return PartialView("Delete", adviserViewModel);
        }

        [HttpPost]
        public JsonResult ConfirmDelete(AdviserViewModel model)
        {
            var result = _adviserService.Delete(model.Id);

            return Json(new { success = result }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult Details(int id)
        {
            var result = _adviserService.GetSingle(id);

            var adviserViewModel = result.Adapt<AdviserViewModel>();

            return PartialView(adviserViewModel);
        }

        public async Task<JsonResult> Save(AdviserViewModel model)
        {
            bool result;

            var context = _adviserService.GetContext();

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var identityRoles = roleMngr.Roles.ToList();

            var roles = model.RoleIds?.DefaultIfEmpty("").Select(x => identityRoles.FirstOrDefault(y => y.Id == x)?.Name);

            if (model.Id > 0)
            {
                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var currentUser = await userManager.FindByIdAsync(model.UserId);

                currentUser.Email = model.User.Email;

                var currentRoles = currentUser.Roles?.Select(y => identityRoles.FirstOrDefault(z => z.Id == y.RoleId)?.Name);

                var userResult = await userManager.UpdateAsync(currentUser);

                if (userResult.Succeeded)
                {
                    if (currentRoles.Any())
                    {
                        userManager.RemoveFromRoles(currentUser.Id, currentRoles.ToArray());
                    }

                    if (roles != null)
                    {
                        foreach (var role in roles)
                        {
                            userManager.AddToRole(currentUser.Id, role);
                        }
                    }
                    var currentAdviser = _adviserService.GetSingle(model.Id);

                    var updatedAdviser = model.Adapt(currentAdviser);
                    updatedAdviser.User = null;

                    result = _adviserService.Update(updatedAdviser);
                }
                else
                {
                    result = false;
                }

            }
            else
            {
                var user = new ApplicationUser { UserName = model.User.Email, Email = model.User.Email };

                var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

                var userManagerResult = await userManager.CreateAsync(user, "12345678");

                if (userManagerResult.Succeeded)
                {
                    if (roles != null)
                    {
                        foreach (var role in roles)
                        {
                            userManager.AddToRole(user.Id, role);
                        }
                    }

                    model.UserId = user.Id;

                    var adviser = model.Adapt<Adviser>();
                    adviser.User = null;

                    result = _adviserService.Add(adviser);
                }
                else
                {
                    result = false;
                }
            }

            return Json(new { success = result });
        }

        public JsonResult List()
        {
            var result = _adviserService.GetAll(includeProperties: x => x.User.Roles).OrderByDescending(x => x.Id);

            var context = _adviserService.GetContext();

            var roleStore = new RoleStore<IdentityRole>(context);
            var roleMngr = new RoleManager<IdentityRole>(roleStore);

            var roleToRemove = roleMngr.Roles.FirstOrDefault(x => x.Name == "Student");

            var roles = roleMngr.Roles.ToList();
            roles.Remove(roleToRemove);

            //var advisers = result.Adapt<IEnumerable<AdviserViewModel>>();

            var advisers = result.Select(x =>
            {
                var dataTableModel = x.Adapt<UserDataTableModel>();

                dataTableModel.Roles = x.User.Roles.Any() ? x.User.Roles.Select(y => roles.FirstOrDefault(z => z.Id == y.RoleId)?.Name).Aggregate((a, b) => string.Join(", ", a, b)) : "";

                return dataTableModel;
            });

            return Json(advisers, JsonRequestBehavior.AllowGet);
        }
    }
}