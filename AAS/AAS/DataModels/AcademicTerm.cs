﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class AcademicTerm
    {
        public int Id { get; set; }
        public Semester Semester { get; set; }
        public int? SchoolYearId { get; set; }
        [ForeignKey("SchoolYearId")]
        public SchoolYear SchoolYear { get; set; }
    }
}