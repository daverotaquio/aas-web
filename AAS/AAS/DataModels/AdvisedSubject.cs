﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class AdvisedSubject
    {
        public int Id { get; set; }
        public int EnrollmentId { get; set; }
        public int StudentId { get; set; }
        public int CurriculumItemId { get; set; }

        [ForeignKey("EnrollmentId")]
        public Enrollment Enrollment { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }
        [ForeignKey("CurriculumItemId")]
        public CurriculumItem CurriculumItem { get; set; }
        public Section? PreferredSection { get; set; }
    }
}