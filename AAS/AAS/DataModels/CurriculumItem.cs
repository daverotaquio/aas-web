﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class CurriculumItem
    {
        public int Id { get; set; }
        public int? CurriculumVersionId { get; set; }
        public int SubjectId { get; set; }
        public Semester? Semester { get; set; }
        public int? CourseId { get; set; }
        public Year? Year { get; set; }
        public int? AdviserId { get; set; }
        [ForeignKey("AdviserId")]
        public Adviser Adviser { get; set; }

        [ForeignKey("CurriculumVersionId")]
        public CurriculumVersion CurriculumVersion { get; set; }
        [ForeignKey("SubjectId")]
        public Subject Subject { get; set; }
        [ForeignKey("CourseId")]
        public Course Course { get; set; }
        public ICollection<CurriculumItemPreRequisites> PreRequisites { get; set; }
        public ICollection<Schedule> Schedules { get; set; }
    }
}