﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AAS.DataModels
{
    public class CurriculumItemPreRequisites
    {
        public int Id { get; set; }
        public int? CurriculumItemtId { get; set; }
        public int? PreRequisiteId { get; set; }

        [ForeignKey("CurriculumItemtId")]
        public CurriculumItem CurriculumItem { get; set; }
        [ForeignKey("PreRequisiteId")]
        public Subject PreRequisite { get; set; }
    }
}