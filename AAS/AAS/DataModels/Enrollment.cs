﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class Enrollment
    {
        public int Id { get; set; }
        public int? StudentId { get; set; }
        public int? AcademicTermId { get; set; }
        public AdmissionStatus? AdmissionStatus { get; set; }
        [ForeignKey("AcademicTermId")]
        public AcademicTerm AcademicTerm { get; set; }
        public Year? Year { get; set; }
        public AcademicStanding? AcademicStanding { get; set; }
        public EnrollmentStatus? EnrollmentStatus { get; set; }
        public int? AdviserId { get; set; }
        [ForeignKey("AdviserId")]
        public Adviser Adviser { get; set; }

        [ForeignKey("StudentId")]
        public Student Student { get; set; }

        public ICollection<AdvisedSubject> AdvisedSubjects { get; set; }

        public Section? Section { get; set; }
    }
}