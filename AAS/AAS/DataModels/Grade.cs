﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class Grade
    {
        [ForeignKey("StudentRecord")]
        public int GradeId { get; set; }
        public double? Midterm { get; set; }
        public GradeValue? Final { get; set; }
        public double? ReExam { get; set; }
        public GradeStatus? Status { get; set; }
        public virtual StudentRecord StudentRecord { get; set; }
    }
}