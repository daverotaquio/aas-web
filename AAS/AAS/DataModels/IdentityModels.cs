﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace AAS.DataModels
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Adviser> Advisers { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<CurriculumItem> CurriculumItems { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<SchoolYear> SchoolYears { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentRecord> StudentRecords { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<AcademicTerm> AcademicTerms { get; set; }
        public DbSet<CurriculumItemPreRequisites> CurriculumItemSubjects { get; set; }
        public DbSet<CurriculumVersion> CurriculumVersions { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<AdvisedSubject> AdvisedSubjects { get; set; }
        public DbSet<Schedule> Schedules { get; set; }
        public DbSet<Room> Rooms { get; set; }
    }
}