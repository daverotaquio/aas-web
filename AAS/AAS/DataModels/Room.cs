﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.DataModels
{
    public class Room
    {
        public int RoomId { get; set; }
        public string RoomName { get; set; }
    }
}