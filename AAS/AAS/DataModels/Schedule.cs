﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class Schedule
    {
        public int ScheduleId { get; set; }
        public int CurriculumItemId { get; set; }
        public DayOfWeek? DayOfWeek { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        [ForeignKey("CurriculumItemId")]
        public CurriculumItem CurriculumItem { get; set; }

        public Section? Section { get; set; }
        public int? RoomId { get; set; }
        [ForeignKey("RoomId")]
        public Room Room { get; set; }
    }
}