﻿using AAS.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AAS.DataModels
{
    public class Student
    {
        public int Id { get; set; }
        public string IDNumber { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public Gender Gender { get; set; }
        public string ContactNumber { get; set; }
        public int? CourseId { get; set; }
        public int? CurriculumVersionId { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }
        [ForeignKey("CourseId")]
        public Course Course { get; set; }
        [ForeignKey("CurriculumVersionId")]
        public CurriculumVersion CurriculumVersion { get; set; }

        public ICollection<Enrollment> Enrollments { get; set; }

        public Year? Year { get; set; }
    }
}