﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataModels
{
    public class StudentRecord
    {
        public int StudentRecordId { get; set; }
        public int StudentId { get; set; }
        public int CurriculumItemId { get; set; }
        public int? EnrollmentId { get; set; }

        [ForeignKey("CurriculumItemId")]
        public CurriculumItem CurriculumItem { get; set; }
        [ForeignKey("EnrollmentId")]
        public Enrollment EnrollmentHistory { get; set; }

        public virtual Grade Grade { get; set; }
        public int? AcademicTermId { get; set; }
        [ForeignKey("AcademicTermId")]
        public AcademicTerm AcademicTerm { get; set; }
    }
}