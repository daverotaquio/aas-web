﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AAS.DataModels
{
    public class Subject
    {
        public int Id { get; set; }
        public string SubjectCode { get; set; }
        public string DescriptiveTitle { get; set; }
        public int Units { get; set; }
    }
}