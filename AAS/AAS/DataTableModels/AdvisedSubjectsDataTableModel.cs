﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.DataTableModels
{
    public class AdvisedSubjectsDataTableModel
    {
        public string SubjectCode { get; set; }
        public int CurriculumItemId { get; set; }
        public string DescriptiveTitle { get; set; }
        public string PreRequisites { get; set; }
        public string FinalGrade { get; set; }
        public string Status { get; set; }
        public string AcademicTerm { get; set; }
        public string Units { get; set; }
        public int Id { get; set; }
        public string YearCourseSection { get; set; }
        public bool HasConflictingSchedule { get; set; }
        public string ConflictingSubjects { get; set; }
        public Section Section { get; set; }
    }
}