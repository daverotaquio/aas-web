﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.DataTableModels
{
    public class AdviserDataTableModel
    {

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string FullName { get; set; }
        public string UserId { get; set; }
        public string Course { get; set; }
    }
}