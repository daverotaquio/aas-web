﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.DataTableModels
{
    public class EnrollmentHistoryDataTableModel
    {
        public string Id { get; set; }
        public string AcademicTerm { get; set; }
        public string StudentId { get; set; }
        public string Year { get; set; }
        public string AcademicStanding { get; set; }
        public string Status { get; set; }
        public string EnrollmentStatus { get; set; }
    }
}