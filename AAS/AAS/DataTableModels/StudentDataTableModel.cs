﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.DataTableModels
{
    public class StudentDataTableModel
    {
        public int Id { get; set; }
        public string IdNumber { get; set; }
        public string FullName { get; set; }
        public string Gender { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string Course { get; set; }
        public string AcademicStanding { get; set; }
        public string Year { get; set; }
    }
}