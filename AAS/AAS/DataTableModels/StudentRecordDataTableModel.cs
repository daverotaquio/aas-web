﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.DataTableModels
{
    public class StudentRecordDataTableModel
    {
        public string SubjectCode { get; set; }
        public string DescriptiveTitle { get; set; }
        public string PreRequisites { get; set; }
        public string FinalGrade { get; set; }
        public string Status { get; set; }
        public string AcademicTerm { get; set; }
        public bool IsAvailable { get; set; }
        public string Units { get; set; }
        public int? Id { get; set; }
        public bool IsAdvised { get; set; }
        public bool IsSemesterMatching { get; set; }
        public string SemesterMatchMessage { get; set; }
        public bool IsYearMatching { get; set; }
        public string YearMatchMessage { get; set; }
        public int? CurriculumItemCourseId { get; set; }
        public int? CurriculumItemId { get; set; }
    }
}