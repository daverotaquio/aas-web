﻿using AAS.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAS.DataTableModels
{
    public class UserDataTableModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string FullName
        {
            get
            {

                return string.Join(" ", FirstName, MiddleName, LastName);
            }
        }
        public string UserId { get; set; }
        public User User { get; set; }
        public string Roles { get; set; }
    }
}