﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum AcademicStanding
    {
        Regular = 1,
        Irregular = 2,
        Shifter = 3,
        Earner = 4
    }
}