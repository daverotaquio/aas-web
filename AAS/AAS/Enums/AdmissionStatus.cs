﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum AdmissionStatus
    {
        [Display(Name = "For Advisement")]
        ForAdvisement = 1,
        [Display(Name = "For Assessment")]
        ForAssessment = 2,
        [Display(Name = "For Admission")]
        ForAdmission = 3,
        [Display(Name = "Enrolled")]
        Enrolled = 4,
        [Display(Name = "Advisement Completed")]
        AdvisementCompleted = 5
    }
}