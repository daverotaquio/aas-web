﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum EnrollmentStatus
    {
        [Display(Name = "Active")]
        Active = 1,
        [Display(Name = "Completed")]
        Completed = 2
    }
}