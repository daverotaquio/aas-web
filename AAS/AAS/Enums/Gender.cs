﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum Gender
    {
        Male = 1,
        Female = 2
    }
}