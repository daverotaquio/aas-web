﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum GradeValue
    {
        [Display(Name = "1.00")]
        One = 1,
        [Display(Name = "1.25")]
        OneTwentyFive = 2,
        [Display(Name = "1.50")]
        OneFifty = 3,
        [Display(Name = "1.75")]
        OneSeventyFive = 4,
        [Display(Name = "2.00")]
        Two = 5,
        [Display(Name = "2.25")]
        TwoTwentyFive = 6,
        [Display(Name = "2.50")]
        TwoFifty = 7,
        [Display(Name = "2.75")]
        TwoSeventyFive = 8,
        [Display(Name = "3.00")]
        Three = 9,
        [Display(Name = "4.00")]
        Four = 10,
        [Display(Name = "5.00")]
        Five = 11,
        [Display(Name = "INC")]
        Incomplete = 12
    }
}