﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum GradeStatus
    {
        Passed = 1,
        Incomplete = 2,
        Failed = 3,
        Unposted = 4,
        Conditional = 5
    }
}