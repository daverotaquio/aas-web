﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum LoadStatus
    {
        Normal = 1,
        Delinquent = 2,
        Probation = 3,
        Warning = 4,
        Debarred = 5
    }
}