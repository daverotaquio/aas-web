﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum Semester
    {
        [Display(Name = "1st Semester")]
        FirstSemster = 1,
        [Display(Name = "2nd Semester")]
        SecondSemester = 2,
        [Display(Name = "Summer")]
        Summer = 3
    }
}