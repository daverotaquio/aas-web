﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum StudentRecordStatus
    {
        Posted = 1,
        Unposted = 2
    }
}