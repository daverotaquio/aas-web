﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAS.Enums
{
    public enum Year
    {
        [Display(Name = "1st Year")]
        First = 1,
        [Display(Name = "2nd Year")]
        Second = 2,
        [Display(Name = "3rd Year")]
        Third = 3,
        [Display(Name = "4th Year")]
        Fourth = 4,
        [Display(Name = "5th Year")]
        Fifth = 5
    }
}