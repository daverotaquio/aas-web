using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using AAS.DataModels;
using AAS.Models;
using Mapster;
using Sentry;

namespace AAS
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            SentrySdk.Init("https://51ed58f70ba44f35bcc1773890889ec5@sentry.io/1332420");

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            TypeAdapterConfig<Schedule, ScheduleViewModel>
                .NewConfig()
                .MaxDepth(4);

            TypeAdapterConfig<EnrollmentViewModel, Enrollment>
                .NewConfig()
                .MaxDepth(4);

            TypeAdapterConfig<Student, StudentViewModel>
                .NewConfig()
                .MaxDepth(4);

            TypeAdapterConfig<AdvisedSubject, AdvisedSubjectViewModel>
                .NewConfig()
                .MaxDepth(4);
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();

            SentrySdk.CaptureEvent(new SentryEvent(exception));
        }
    }
}
