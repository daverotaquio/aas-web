﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AAS.App_Start;
using AAS.ApplicationServices.Interface;
using AAS.DataModels;
using AAS.Enums;
using Ninject;

namespace AAS.Helpers
{
    public static class AdvisementHelper
    {
        public static AdvisedSubjectInfo GetAllowedUnits(int studentId)
        {
            var advisedSubjectInfo = new AdvisedSubjectInfo();

            var kernel = NinjectWebCommon.bootstrapper.Kernel;

            var studentRecordService = kernel.Get<IService<StudentRecord>>();
            var enrollmentService = kernel.Get<IService<Enrollment>>();

            var enrollments = enrollmentService.GetAll(x => x.StudentId == studentId).ToList();

            var activeEnrollment = enrollments.FirstOrDefault(x => x.EnrollmentStatus == EnrollmentStatus.Active) ?? enrollments.LastOrDefault();

            int? previousEnrollmentIndex = null;

            if (enrollments.Count > 1 && activeEnrollment != null)
            {
                previousEnrollmentIndex = enrollments.IndexOf(activeEnrollment) - 1;
            }
            else
            {
                previousEnrollmentIndex = 0;
            }

            var previousEnrollment = enrollments.Count > 1 ? previousEnrollmentIndex != null ? enrollments.ElementAt(previousEnrollmentIndex.GetValueOrDefault()) : new Enrollment() : new Enrollment() ?? new Enrollment();

            var previousEnrollmentStudentRecords = studentRecordService.GetAll(x => x.EnrollmentId == previousEnrollment.Id, includeProperties: x => x.CurriculumItem.Subject).ToList();

            var totalUnitsFromPreviousEnrollment = enrollments.Count > 1 ? previousEnrollmentStudentRecords.Select(x => x.CurriculumItem.Subject.Units).Sum() : 25;

            var totalPassedUnits = enrollments.Count > 1 ? previousEnrollmentStudentRecords.Where(x => x.Grade.Status == GradeStatus.Passed).Select(x => x.CurriculumItem.Subject.Units).Sum() : 25;

            var result = totalPassedUnits != 0 || totalUnitsFromPreviousEnrollment != 0 ? Math.Round(Convert.ToDouble(totalPassedUnits) / Convert.ToDouble(totalUnitsFromPreviousEnrollment), 2) : 0.0;

            var failedUnitsPercentage = enrollments.Count > 1 ? Convert.ToInt32(100 - (result * 100)) : 0;

            if (failedUnitsPercentage <= 25 && failedUnitsPercentage >= 1)
            {
                advisedSubjectInfo.LoadStatus = LoadStatus.Delinquent;
                advisedSubjectInfo.AllowedUnits = totalUnitsFromPreviousEnrollment;
            }
            else if (failedUnitsPercentage > 25 && failedUnitsPercentage <= 50)
            {
                advisedSubjectInfo.LoadStatus = LoadStatus.Probation;
                advisedSubjectInfo.AllowedUnits = 15;
            }
            else if (failedUnitsPercentage > 50 && failedUnitsPercentage <= 75)
            {
                advisedSubjectInfo.LoadStatus = LoadStatus.Warning;
                advisedSubjectInfo.AllowedUnits = 12;
            }
            else if (failedUnitsPercentage > 75)
            {
                advisedSubjectInfo.LoadStatus = LoadStatus.Debarred;
                advisedSubjectInfo.AllowedUnits = 9;
            }
            else
            {
                advisedSubjectInfo.LoadStatus = LoadStatus.Normal;
                advisedSubjectInfo.AllowedUnits = totalUnitsFromPreviousEnrollment;
            }

            advisedSubjectInfo.TotalPassedUnits = totalPassedUnits;
            advisedSubjectInfo.FailedUnitsPercentage = failedUnitsPercentage;
            advisedSubjectInfo.TotalUnitsFromPreviousEnrollment = totalUnitsFromPreviousEnrollment;

            return advisedSubjectInfo;
        }
    }

    public class AdvisedSubjectInfo
    {
        public int TotalPassedUnits { get; set; }
        public int TotalUnitsFromPreviousEnrollment { get; set; }
        public int FailedUnitsPercentage { get; set; }
        public int AllowedUnits { get; set; }
        public LoadStatus LoadStatus { get; set; }
    }
}