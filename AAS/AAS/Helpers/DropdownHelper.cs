﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Helpers
{
    public static class DropdownHelper
    {
        private static readonly List<double> grades = new List<double>
        {
            1,
            1.25,
            1.5,
            1.75,
            2,
            2.25,
            2.5,
            2.75,
            3,
            4,
            5
        };

        public static IEnumerable<double> Grades
        {
            get
            {
                return grades;
            }
        }
    }
}