﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace AAS.Helpers
{
    public static class EnumHelper
    {
        /// <summary>
        ///     A generic extension method that aids in reflecting 
        ///     and retrieving any attribute that is applied to an `Enum`.
        /// </summary>
        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
                where TAttribute : Attribute
        {

            try
            {
                return enumValue.GetType()
                                .GetMember(enumValue.ToString())?
                                .FirstOrDefault()?
                                .GetCustomAttribute<TAttribute>();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static string GetShortDayOfWeek(this DayOfWeek dayOfWeek)
        {
            var result = "";

            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    result = "Su";
                    break;
                case DayOfWeek.Monday:
                    result = "M";
                    break;
                case DayOfWeek.Tuesday:
                    result = "T";
                    break;
                case DayOfWeek.Wednesday:
                    result = "W";
                    break;
                case DayOfWeek.Thursday:
                    result = "Th";
                    break;
                case DayOfWeek.Friday:
                    result = "F";
                    break;
                case DayOfWeek.Saturday:
                    result = "S";
                    break;
                default:
                    result = "";
                    break;
            }

            return result;
        }
    }
}