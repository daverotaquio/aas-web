﻿using System;
using System.ComponentModel.DataAnnotations;
using AAS.Enums;
using AAS.Helpers;

namespace AAS.Models
{
    public class AcademicTermViewModel
    {
        public int Id { get; set; }
        public Semester Semester { get; set; }
        public int SchoolYearId { get; set; }
        public SchoolYearViewModel SchoolYear { get; set; }
        public string FullName
        {
            get
            {
                return SchoolYear != null ? $"{SchoolYear.Description}, {Semester.GetAttribute<DisplayAttribute>().Name}" : "";
            }
        }
    }
}