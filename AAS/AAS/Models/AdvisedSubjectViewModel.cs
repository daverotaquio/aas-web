﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.Models
{
    public class AdvisedSubjectViewModel
    {
        public int Id { get; set; }
        public int EnrollmentId { get; set; }
        public int StudentId { get; set; }
        public int CurriculumItemId { get; set; }

        public EnrollmentViewModel Enrollment { get; set; }
        public StudentViewModel Student { get; set; }
        public CurriculumItemViewModel CurriculumItem { get; set; }
        public IEnumerable<int> AdvisedSubjectIds { get; set; }
        public Section? PreferredSection { get; set; }
    }
}