﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AAS.Helpers;

namespace AAS.Models
{
    public class AdvisementViewModel
    {
        public StudentViewModel Student { get; set; }
        public IEnumerable<StudentRecordViewModel> StudentRecords { get; set; }
        public IEnumerable<StudentRecordViewModel> OutstandingStudentRecords { get; set; }
        public AdvisedSubjectInfo AdvisedSubjectInfo { get; set; }
        public IndexPageViewModel IndexPageViewModel { get; set; }
    }
}