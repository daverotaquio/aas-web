﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAS.Models
{
    public class AdviserViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string ContactNumber { get; set; }
        public string FullName
        {
            get
            {

                return string.Join(" ", FirstName, MiddleName, LastName);
            }
        }
        public string UserId { get; set; }
        public User User { get; set; }
        public List<string> RoleIds { get; set; }
        [Display(Name = "Roles")]
        public IEnumerable<SelectListItem> Roles { get; set; }

        [Display(Name = "Adviser for Course")]
        public IEnumerable<SelectListItem> Courses { get; set; }
        [Required]
        [Display(Name = "Course")]
        public int? CourseId { get; set; }
        public CourseViewModel Course { get; set; }
    }
}