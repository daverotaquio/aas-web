﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class AlternativeSubjectsViewModel
    {
        public int AlternativeSubjectReferenceId { get; set; }
        public int AlternativeSubjectEnrollmentId { get; set; }
        public int AlternativeSubjectStudentId { get; set; }
        public int AlternativeSubjectCurriculumItemId { get; set; }
        public int AlternativeSubjectSection { get; set; }
    }
}