﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class CurriculumItemPreRequisitesViewModel
    {
        public int Id { get; set; }
        public int CurriculumItemtId { get; set; }
        public int PreRequisiteId { get; set; }
        public CurriculumItemViewModel CurriculumItem { get; set; }
        public SubjectViewModel PreRequisite { get; set; }
    }
}