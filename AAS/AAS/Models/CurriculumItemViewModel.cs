﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.Enums;
using AAS.Helpers;

namespace AAS.Models
{
    public class CurriculumItemViewModel
    {

        public int Id { get; set; }
        [Required]
        public int? CurriculumVersionId { get; set; }
        [Required]
        public int SubjectId { get; set; }
        [Required]
        public Semester? Semester { get; set; }
        [Required]
        public int? CourseId { get; set; }
        [Required]
        public Year? Year { get; set; }
        public IEnumerable<int?> PreRequisiteIds { get; set; }
        public int? AdviserId { get; set; }
        public AdviserViewModel Adviser { get; set; }
        public CurriculumVersionViewModel CurriculumVersion { get; set; }
        public SubjectViewModel Subject { get; set; }
        public CourseViewModel Course { get; set; }

        [Display(Name = "Subject")]
        public IEnumerable<SelectListItem> Subjects { get; set; }
        [Display(Name = "Course")]
        public IEnumerable<SelectListItem> Courses { get; set; }
        [Display(Name = "Curriculum")]
        public IEnumerable<SelectListItem> CurriculumVersions { get; set; }
        [Display(Name = "Pre-requisites")]
        public IEnumerable<SelectListItem> PreRequisite { get; set; }
        [Display(Name = "Adviser")]
        public IEnumerable<SelectListItem> Advisers { get; set; }


        public string YearString
        {
            get
            {
                return Year.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name ?? string.Empty;
            }
        }

        public string SemesterString
        {
            get
            {
                return Semester.GetValueOrDefault().GetAttribute<DisplayAttribute>()?.Name ?? string.Empty;
            }
        }

        public ICollection<CurriculumItemPreRequisitesViewModel> PreRequisites { get; set; }

        public string FullName
        {
            get
            {
                return $"{Subject.SubjectCode} - {Subject.DescriptiveTitle}";
            }
        }


        public ICollection<ScheduleViewModel> Schedules { get; set; }
        public ScheduleViewModel Schedule { get; set; }
        [Display(Name = "Room")]
        public IEnumerable<SelectListItem> Rooms { get; set; }
    }
}