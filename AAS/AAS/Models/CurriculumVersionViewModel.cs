﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class CurriculumVersionViewModel
    {
        public int Id { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
    }
}