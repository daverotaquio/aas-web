﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.Enums;
using AAS.Helpers;

namespace AAS.Models
{
    public class EnrollmentViewModel
    {

        public int Id { get; set; }
        public int? StudentId { get; set; }
        [Required]
        [Display(Name = "Academic Term")]
        public int? AcademicTermId { get; set; }
        [Display(Name = "Admission Status")]
        public AdmissionStatus? AdmissionStatus { get; set; }
        [Required]
        public int? AdviserId { get; set; }
        public AdviserViewModel Adviser { get; set; }
        public AcademicTermViewModel AcademicTerm { get; set; }

        [Required]
        public Year? Year { get; set; }

        [Display(Name = "Academic Term")]
        public IEnumerable<SelectListItem> AcademicTerms { get; set; }


        public string FullName
        {
            get
            {
                return $"{AcademicTerm?.SchoolYear?.Description}, {AcademicTerm?.Semester.GetAttribute<DisplayAttribute>()?.Name}" ?? "";
            }
        }
        [Required]
        [Display(Name = "Academic Standing")]
        public AcademicStanding? AcademicStanding { get; set; }
        [Display(Name = "Enrollment Status")]
        public EnrollmentStatus? EnrollmentStatus { get; set; }
        [Required]
        public IEnumerable<SelectListItem> Advisers { get; set; }
        [Required]
        public Section? Section { get; set; }
        public StudentViewModel Student { get; set; }
        public AdvisedSubjectInfo AdvisedSubjectInfo { get; set; }
    }

    public class EvaluationEnrollmentViewModel : EnrollmentViewModel
    {
        [Display(Name = "Academic Term")]
        public new int? AcademicTermId { get; set; }
    }
}