﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AAS.Enums;

namespace AAS.Models
{
    public class GradeViewModel
    {
        public int GradeId { get; set; }
        public double? Midterm { get; set; }
        [Required]
        public GradeValue? Final { get; set; }
        [Display(Name = "Re-exam")]
        public double? ReExam { get; set; }
        [Required]
        public GradeStatus? Status { get; set; }
        public IEnumerable<double> GradesSelection { get; set; }
    }
}