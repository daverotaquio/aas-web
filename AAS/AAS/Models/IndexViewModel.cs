﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class IndexPageViewModel
    {
        public string CurrentUserRole { get; set; }
    }
}