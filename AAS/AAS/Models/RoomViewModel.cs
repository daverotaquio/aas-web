﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class RoomViewModel
    {
        public int RoomId { get; set; }
        public string RoomName { get; set; }
    }
}