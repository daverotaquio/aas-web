﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.Enums;

namespace AAS.Models
{
    public class ScheduleViewModel
    {
        public int ScheduleId { get; set; }
        public int CurriculumItemId { get; set; }
        [Required]
        [Display(Name = "Day of Week")]
        public DayOfWeek? DayOfWeek { get; set; }
        [Required]
        [Display(Name = "Start Time")]
        public string StartTime { get; set; }
        [Required]
        [Display(Name = "End Time")]
        public string EndTime { get; set; }
        public CurriculumItemViewModel CurriculumItem { get; set; }
        [Required]
        public Section Section { get; set; }
        public int? RoomId { get; set; }
        [Display(Name = "Room")]
        public IEnumerable<SelectListItem> Rooms { get; set; }
        public RoomViewModel Room { get; set; }
        public List<DayOfWeek> Days { get; set; }
    }
}