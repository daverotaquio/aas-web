﻿namespace AAS.Models
{
    public class SchoolYearViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}