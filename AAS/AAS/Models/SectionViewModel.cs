﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class SectionViewModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}