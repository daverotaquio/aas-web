﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.Enums;

namespace AAS.Models
{
    public class StudentRecordViewModel
    {
        public int Id { get; set; }
        public int StudentId { get; set; }
        public int CurriculumItemId { get; set; }
        public int? EnrollmentId { get; set; }

        public CurriculumItemViewModel CurriculumItem { get; set; }
        public GradeViewModel Grade { get; set; }
        public EnrollmentViewModel EnrollmentHistory { get; set; }
        [Display(Name = "Subject")]
        public IEnumerable<SelectListItem> CurriculumItems { get; set; }
        [Display(Name = "Academic Term")]
        public IEnumerable<SelectListItem> EnrolledAcademicTerms { get; set; }
        [Display(Name = "Academic Term")]
        public IEnumerable<SelectListItem> AcademicTerms { get; set; }
        public AcademicTermViewModel AcademicTerm { get; set; }

        public bool IsCreate { get; set; }
        [Required]
        public int? AcademicTermId { get; set; }
    }
}