﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AAS.Enums;

namespace AAS.Models
{
    public class StudentViewModel
    {

        public int Id { get; set; }
        [Required]
        [Display(Name = "ID Number")]
        public string IDNumber { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public Gender? Gender { get; set; }
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public int? AdviserId { get; set; }
        [Required]
        public int? CourseId { get; set; }
        [Required]
        public int? CurriculumVersionId { get; set; }
        public string UserId { get; set; }
        public string FullName
        {
            get
            {
                return string.Join(" ", FirstName, MiddleName, LastName);
            }
        }
        public string GenderString
        {
            get
            {
                return Enum.GetName(typeof(Gender), Gender);
            }
        }

        public User User { get; set; }
        [Display(Name = "Course")]
        public IEnumerable<SelectListItem> Courses { get; set; }
        [Display(Name = "Curriculum")]
        public IEnumerable<SelectListItem> CurriculumVersions { get; set; }
        public CourseViewModel Course { get; set; }
        public CurriculumVersionViewModel CurriculumVersion { get; set; }

        public IEnumerable<EnrollmentViewModel> Enrollments { get; set; }
        [Required]
        public Year? Year { get; set; }
    }
}