﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AAS.Models
{
    public class SubjectViewModel
    {
        public int Id { get; set; }
        [Required]
        [Display(Name = "Subject Code")]
        public string SubjectCode { get; set; }
        [Required]
        [Display(Name = "Descriptive Title")]
        public string DescriptiveTitle { get; set; }
        [Range(1, 6)]
        public int Units { get; set; }
        public IEnumerable<int?> PreRequisiteIds { get; set; }
        public IEnumerable<SelectListItem> PreRequisite { get; set; }
        public string FullName
        {
            get
            {
                return $"{SubjectCode} - {DescriptiveTitle}";
            }
        }
    }
}