﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AAS.Enums;

namespace AAS.Models
{
    public class TrialProgramViewModel
    {
        public string IdNumber { get; set; }
        public string FullName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string YearCourseSection { get; set; }
        public AcademicStanding AcademicStanding { get; set; }
        public List<ScheduleViewModel> Schedules { get; set; }
        public List<AdvisedSubjectViewModel> AdvisedSubjects { get; set; }
        public string Adviser { get; set; }
    }
}