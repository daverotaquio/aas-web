﻿using AAS.DataModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace AAS.Models
{
    public class User
    {
        public string Id { get; set; }
        [Required]
        public string Email { get; set; }
    }
}