﻿using AAS.DataModels;
using AAS.Persistence.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace AAS.Persistence.Concrete
{
    public class DataManager<T> : IDataManager<T> where T : class
    {
        internal ApplicationDbContext _context;
        internal DbSet<T> _dbset;

        public DataManager(ApplicationDbContext context)
        {
            _context = context;
            _dbset = context.Set<T>();
        }

        public ApplicationDbContext GetContext()
        {
            if (_context != null)
            {
                return _context;
            }
            else
            {
                return null;
            }
        }

        public IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbset;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
                query = orderBy(query);

            return enableTracking ? query.ToList() : query.AsNoTracking().ToList();
        }


        public IEnumerable<T> GetJoined(
            Expression<Func<T, bool>> filter = null,
            Expression<Func<T, T>> select = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbset;

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (select != null)
            {
                query.Select(select);
            }

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
                query = orderBy(query);

            return enableTracking ? query.ToList() : query.AsNoTracking().ToList();
        }

        public IEnumerable<T> DeferredGet(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = _dbset;


            if (filter != null)
            {
                query = query.Where(filter);
            }

            query = includeProperties.Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null)
                query = orderBy(query);

            return enableTracking ? query : query.AsNoTracking();
        }

        public T GetById(object id)
        {
            return _dbset.Find(id);
        }

        public T SignleOrDefault(Expression<Func<T, bool>> predicate)
        {
            return _dbset.Where(predicate).FirstOrDefault();
        }

        public T Add(T entity)
        {
            _dbset.Add(entity);
            _context.SaveChanges();

            return entity;
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _dbset.AddRange(entities);
            _context.SaveChanges();
        }

        public void Remove(T entity)
        {
            _dbset.Remove(entity);
            _context.SaveChanges();
        }

        public void RemoveRange(Expression<Func<T, bool>> predicate)
        {
            var entities = _dbset.Where(predicate);
            _dbset.RemoveRange(entities);
            _context.SaveChanges();
        }

        public T Update(T entityToUpdate)
        {
            _dbset.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;
            _context.SaveChanges();

            return entityToUpdate;
        }

        public void Remove(object id)
        {
            var entity = _dbset.Find(id);
            _dbset.Remove(entity);
            _context.SaveChanges();
        }
    }
}