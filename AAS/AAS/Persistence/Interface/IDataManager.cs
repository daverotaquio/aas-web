﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace AAS.Persistence.Interface
{
    public interface IDataManager<T> where T : class
    {
        IEnumerable<T> Get(
            Expression<Func<T, bool>> filter = null,
            Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            bool enableTracking = false,
            params Expression<Func<T, object>>[] includeProperties);
        T GetById(object id);
        T SignleOrDefault(Expression<Func<T, bool>> predicate);
        T Add(T entity);
        void AddRange(IEnumerable<T> entities);
        void Remove(object id);
        void Remove(T entity);
        T Update(T entityToUpdate);
        void RemoveRange(Expression<Func<T, bool>> predicate);
        IEnumerable<T> DeferredGet(Expression<Func<T, bool>> filter = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, bool enableTracking = false, params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> GetJoined(Expression<Func<T, bool>> filter = null, Expression<Func<T, T>> select = null, Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null, bool enableTracking = false, params Expression<Func<T, object>>[] includeProperties);
        DataModels.ApplicationDbContext GetContext();
    }
}
