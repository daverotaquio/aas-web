﻿"use strict";

var common = {
    initGeneralSelect2: function () {
        $(document).find(".select2-general").select2({
            width: "100%",
            placeholder: "Nothing Selected",
            dropdownParent: $("#app-modal .modal-content"),
            sorter: function (data) {
                return data.sort();
            }
        });
    },
    setActive: function (id) {
        $("#" + id).addClass("kt-menu__item--here");
    },
    currentUserRole: function () {
        var currentUserRole = $("#current-user-role").val();

        return currentUserRole;
    }
};

var dataTable = {
    dataTableOptions: {
        showEdit: true,
        showDetails: true,
        showDelete: true,
        showEditOutstandingSubject: false,
        showLargeEdit: false,
        showAdvisement: false,
        showEditEnrollmentHistory: false,
        showAlternatives: false,
        showNothing: false
    },
    setDataTableAction: function (data) {
        var isUnauthorizeEdit = $("#unauthorize-edit").val();
        var isUnauthorizeDelete = $("#unauthorize-delete").val();

        var content = `<span class="dropdown">
                            <a href="#" class="btn btn-sm btn-clean btn-icon btn-icon-md" data-toggle="dropdown" aria-expanded="false">
                              <i class="la la-angle-down"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-left" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(0px, 100px, 0px);" x-out-of-boundaries="">
                                ` + (dataTable.dataTableOptions.showAdvisement ? `<a class="dropdown-item" href="/students/advisement/` + data + `"><i class="la la-info"></i> Advisement</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showEdit ? isUnauthorizeEdit ? `<a class="dropdown-item unauthorized" href="_"><i class="la la-edit"></i> Edit</a>` : `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="edit" data-id="` + data + `" href="_"><i class="la la-edit"></i> Edit</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showLargeEdit ? isUnauthorizeEdit ? `<a class="dropdown-item unauthorized" href="_"><i class="la la-edit"></i> Edit</a>` : `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="edit" data-id="` + data + `" data-show-large-modal="true" href="_"><i class="la la-edit"></i> Edit</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showDetails ? `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="details" data-id="` + data + `" href="_"><i class="la la-info"></i> Details</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showDelete ? isUnauthorizeDelete ? `<a class="dropdown-item unauthorized" href="_"><i class="la la-trash"></i> Delete</a>` : `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="delete" data-id="` + data + `" href="_"><i class="la la-trash"></i> Delete</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showEditOutstandingSubject ? `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="edit-outstanding-subjects" data-id="` + data + `" href="_"><i class="la la-edit"></i> Edit</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showEditEnrollmentHistory ? `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="edit" data-id="` + data + `" data-controller="enrollmenthistory" href="_"><i class="la la-edit"></i> Edit</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showAlternatives ? `<a class="dropdown-item" data-toggle="modal" data-target="#app-modal" data-action="alternatives" data-id="` + data + `" data-show-large-modal="true" data-controller="curriculumitems" href="_"><i class="la la-search"></i> Search Alternatives</a>` : ``) + `
                                ` + (dataTable.dataTableOptions.showNothing ? `<a class="dropdown-item" href="javascript:"><i class="la la-info"></i> Nothing here</a>` : ``) + `
                            </div>
                       </span>`;

        dataTable.dataTableAction = content;
    },
    dataTableAction: ""
};

var modal = {
    controller: "",
    headerText: "",
    properties: {
        createModalContentUrl: "",
        createModalContentUrlParams: "",
        editModalContentUrl: "",
        deleteModalContentUrl: "",
        detailsModalContentUrl: "",
        createModalTitle: "",
        editModalTitle: "",
        deleteModalTitle: "",
        detailsModalTitle: "",
        saveModalDataUrl: "",
        deleteModalDataUrl: "",
        customModalDataUrl: "",
        customModalTitle: "",
        customModalButtonTitle: "",
        customModalPostUrl: ""
    },
    getUrl: null,
    modalTitle: null,
    buttonText: null,
    buttonClass: null,
    postUrl: null,
    modal: null,
    notificationTitle: null,
    notificationMessage: null,
    notificationType: null,
    notificationIcon: null,
    action: null,
    performPost: null,
    shownCallback: null,
    hiddenCallback: null,
    setController: null,
    blockPost: null,
    blockPostErrorMessage: null,
    instances: {
        dataTable: null
    },
    setProperties: function () {
        modal.properties.createModalContentUrl = modal.properties.createModalContentUrlParams === "" ? "/" + modal.controller + "/create" : "/" + modal.controller + "/create/" + modal.properties.createModalContentUrlParams;
        modal.properties.editModalContentUrl = "/" + modal.controller + "/edit/";
        modal.properties.deleteModalContentUrl = "/" + modal.controller + "/delete/";
        modal.properties.detailsModalContentUrl = "/" + modal.controller + "/details/";
        modal.properties.createModalTitle = "Add " + modal.headerText + "";
        modal.properties.editModalTitle = "Edit " + modal.headerText + "";
        modal.properties.deleteModalTitle = "Delete " + modal.headerText + "";
        modal.properties.detailsModalTitle = "" + modal.headerText.charAt(0).toUpperCase() + modal.headerText.slice(1) + " details";
        modal.properties.saveModalDataUrl = "/" + modal.controller + "/save";
        modal.properties.deleteModalDataUrl = "/" + modal.controller + "/confirmdelete";

    },
    setGetParams: function (caller) {
        var action = $(caller).data("action");
        var entityId = $(caller).data("id");

        modal.action = action;

        switch (action) {
            case "add":
                modal.getUrl = modal.properties.createModalContentUrl;
                modal.modalTitle = modal.properties.createModalTitle;
                modal.buttonText = "Save";
                modal.buttonClass = "btn-primary";
                break;
            case "edit":
                modal.getUrl = modal.properties.editModalContentUrl + entityId;
                modal.modalTitle = modal.properties.editModalTitle;
                modal.buttonText = "Save";
                modal.buttonClass = "btn-primary";
                break;
            case "edit-outstanding-subjects":
                modal.getUrl = modal.properties.editModalContentUrl + entityId + `?d="1"`;
                modal.modalTitle = modal.properties.editModalTitle;
                modal.buttonText = "Save";
                modal.buttonClass = "btn-primary";
                break;
            case "delete":
                modal.getUrl = modal.properties.deleteModalContentUrl + entityId;
                modal.modalTitle = modal.properties.deleteModalTitle;
                modal.buttonText = "Delete";
                modal.buttonClass = "btn-danger";
                break;
            case "details":
                modal.getUrl = modal.properties.detailsModalContentUrl + entityId;
                modal.modalTitle = modal.properties.detailsModalTitle;
                modal.buttonText = "Done";
                modal.buttonClass = "btn-primary";
                break;
            case "alternatives":
                modal.getUrl = modal.properties.customModalDataUrl;
                modal.modalTitle = modal.properties.customModalTitle;
                modal.buttonText = modal.properties.customModalButtonTitle;
                modal.buttonClass = "btn-primary";
                break;
            default:
                break;
        }
    },
    setPostParams: function () {
        switch (modal.action) {
            case "add":
            case "edit":
            case "edit-outstanding-subjects":
                modal.postUrl = modal.properties.saveModalDataUrl;
                modal.notificationTitle = "<b>Success</b>";
                modal.notificationMessage = "Record successfully saved";
                modal.notificationType = "success";
                modal.notificationIcon = "icon la la-check-circle";
                modal.performPost = true;
                break;
            case "alternatives":
                modal.postUrl = modal.properties.customModalPostUrl;
                modal.notificationTitle = "<b>Success</b>";
                modal.notificationMessage = "Alernative subject successfully added";
                modal.notificationType = "success";
                modal.notificationIcon = "icon la la-check-circle";
                modal.performPost = true;
                break;
            case "delete":
                modal.postUrl = modal.properties.deleteModalDataUrl;
                modal.notificationTitle = "<b>Success</b>";
                modal.notificationMessage = "Record successfully deleted";
                modal.notificationType = "success";
                modal.notificationIcon = "icon la la-check-circle";
                modal.performPost = true;
                break;
            case "details":
                modal.performPost = false;
                break;
            default:
                break;
        }
    },
    clear: function () {
        modal.getUrl = null;
        modal.modalTitle = null;
        modal.buttonText = null;
        modal.buttonClass = null;
        modal.postUrl = null;
        modal.modal = null;
        modal.notificationTitle = null;
        modal.notificationMessage = null;
        modal.notificationType = null;
        modal.notificationIcon = null;
        modal.action = null;
    },
    setup: function () {
        $(document).on("click", ".unauthorized", function (e) {
            e.preventDefault();

            swal.fire({
                title: "Unuthorized",
                text: "You are not allowed to perform this action",
                type: "error",
                showCancelButton: false,
                confirmButtonText: "Close"
            }).then(function (e) {
                e.value;
            });
        });

        $(document).on("show.bs.modal", "#app-modal", function (e) {
            var target;

            if (e.relatedTarget !== undefined) {
                target = e.relatedTarget;
            } else {
                target = e.target;
            }

            var isUnauthorized = $(target).data("is-unauthorized");

            if (isUnauthorized !== undefined) {
                $(this).removeAttr("data-toggle");

                swal.fire({
                    title: "Unuthorized",
                    text: "You are not allowed to perform this action",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonText: "Close"
                }).then(function (e) {
                    e.value;
                });
            }
            //var isUnauthorized = $(target).data("is-unauthorized");

            //if (isUnauthorized !== undefined) {
            //    swal.fire({
            //        title: "Unuthorized",
            //        text: "You are not allowed to perform this action",
            //        type: "error",
            //        showCancelButton: false,
            //        confirmButtonText: "Close"
            //    }).then(function (e) {
            //        e.value;
            //    });

            //    return e;
            //}

            if (modal.setController !== null) {
                modal.setController(e);
            }

            var showLargeModal = $(target).data("show-large-modal");

            if (showLargeModal !== undefined) {
                $("#app-modal .modal-dialog").addClass("modal-lg");
            }

            modal.setGetParams(target);
            modal.get(modal.getUrl, modal.modalTitle, modal.buttonText, modal.buttonClass);

            $(".post-button-modal").data("action", modal.action);
        });

        $(document).on("shown.bs.modal", "#app-modal", function (e) {
            $(".post-button-modal").data("action", modal.action);

            var form = $($(document).find("#modal-form")[0])
                .removeData("validator") /* added by the raw jquery.validate plugin */
                .removeData("unobtrusiveValidation");  /* added by the jquery unobtrusive plugin*/

            $.validator.unobtrusive.parse(form);

            $(form).validate();
        });

        $(document).on('hidden.bs.modal', function () {
            modal.clear();

            $(".modal-title").text("Loading data...");
            $(".modal-body").html(
                `<div class="progress" style="margin: 50px auto 50px auto; width: 50%">
                    <div class="progress-bar progress-bar-striped progress-bar-animated " role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                </div>`
            );

            $(".post-button-modal").removeData("action");
            $(".post-button-modal").text("...");
            $(".post-button-modal").removeClass("btn-primary").removeClass("btn-danger").addClass("btn-secondary");

            $(".post-button-modal").attr("disabled", "disabled");

            $("#app-modal .modal-dialog").removeClass("modal-lg");
        });

        $(document).on("click", ".post-button-modal", function (e) {

            var form = $($(document).find("#modal-form")[0])
                .removeData("validator") /* added by the raw jquery.validate plugin */
                .removeData("unobtrusiveValidation");  /* added by the jquery unobtrusive plugin*/

            $.validator.unobtrusive.parse(form);

            if (modal.blockPost !== null && modal.blockPost === true) {
                $.notify(
                    {
                        title: "Error",
                        message: modal.blockPostErrorMessage
                    },
                    {
                        type: "danger",
                        delay: 3000,
                        timer: 3000
                    }
                );

                return false;
            }

            modal.setPostParams();

            if ($($(document).find("#modal-form")[0]).valid()) {
                if (modal.performPost) {
                    modal.post(modal.postUrl, "modal-form").then(response => {
                        if (response.success) {
                            $("#app-modal").modal("hide");

                            if (modal.hiddenCallback !== null) {
                                modal.hiddenCallback(e);
                            }

                            $.notify(
                                {
                                    title: modal.notificationTitle,
                                    message: modal.notificationMessage,
                                    icon: modal.notificationIcon
                                },
                                {
                                    type: modal.notificationType,
                                    delay: 3000,
                                    timer: 3000
                                }
                            );
                        }
                        else {
                            $.notify(
                                {
                                    title: "Error",
                                    message: "Something went wrong"
                                },
                                {
                                    type: "danger",
                                    delay: 3000,
                                    timer: 3000
                                }
                            );
                        }
                    });
                }
                else {
                    $("#app-modal").modal("hide");
                }
            }
        });
    },
    get: function (url, modalTitle, buttonText, buttonType) {
        fetch(url, {
            method: "GET"
        }).then(response => {
            return response.text();
        }).then(content => {
            $(".modal-title").text(modalTitle);
            $(".modal-body").html(content);

            if (modal.shownCallback !== null) {
                modal.shownCallback();
            }

            if ($(".select2-general")) {
                common.initGeneralSelect2();
            }

            if ($(".select2-grade-status") || $(".select2-final-grade")) {
                if (typeof studentsAdvisement !== 'undefined') {
                    studentsAdvisement.initStudentRecordSelect2();
                }
            }

            if ($(".select2-academic-standing")) {
                if (typeof studentsAdvisement !== 'undefined') {
                    studentsAdvisement.initAcademicStandingSelect2();
                }
            }

            $(".post-button-modal").text(buttonText);
            $(".post-button-modal").removeClass("btn-secondary").removeClass("btn-primary").removeClass("btn-danger").addClass(buttonType);
            $(".post-button-modal").removeAttr("disabled");
        });
    },
    post: function (url, form) {
        var formData = new FormData(document.getElementById(form));

        return fetch(url, {
            method: "POST",
            body: formData
        }).then(response => response.json());
    }
};
