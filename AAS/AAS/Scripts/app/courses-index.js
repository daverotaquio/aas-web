﻿"use strict";

common.setActive("courses");

$().ready(function () {
    coursesIndex.init();
});

var coursesIndex = {
    init: function () {
        modal.controller = "courses";
        modal.headerText = "course";
        modal.setProperties();

        coursesIndex.initDataTable();
        modal.instances.dataTable = coursesIndex.table;

        modal.hiddenCallback = function () {
            coursesIndex.reloadTable();
        };

        modal.setup();
    },
    table: null,
    initDataTable: function () {
        coursesIndex.table = $(".table").DataTable({
            responsive: true,
            order: [0, "desc"],
            ajax: {
                url: "/" + modal.controller + "/list",
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    visible: false,
                    orderable: false
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showDelete = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                { data: "Name" },
                { data: "ShortDescription" }
            ]
        });
    },
    reloadTable: function () {
        coursesIndex.table.ajax.reload(null, false);
    }
};