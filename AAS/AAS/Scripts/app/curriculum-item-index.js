﻿"use strict";

common.setActive("curriculum");

$().ready(function () {
    curriculumItemIndex.init();
});

var curriculumItemIndex = {
    init: function () {
        modal.controller = "curriculumitems";
        modal.headerText = "Curriculum Item";
        modal.setProperties();

        curriculumItemIndex.initDataTable();
        modal.instances.dataTable = curriculumItemIndex.table;

        modal.hiddenCallback = function () {
            curriculumItemIndex.reloadTable();
        };

        modal.shownCallback = function () {
            curriculumItemIndex.callbacks();
        };

        modal.setup();
    },
    table: null,
    initDataTable: function () {
        curriculumItemIndex.table = $(".table").DataTable({
            responsive: true,
            order: [0, "desc"],
            ajax: {
                url: "/" + modal.controller + "/list",
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    visible: false,
                    orderable: false
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showDelete = false;
                        dataTable.dataTableOptions.showDetails = false;
                        dataTable.dataTableOptions.showEdit = false;
                        dataTable.dataTableOptions.showLargeEdit = true;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                { data: "Subject.FullName" },
                { data: "YearString" },
                { data: "SemesterString" },
                { data: "Course.ShortDescription" },
                { data: "CurriculumVersion.ShortDescription" }
            ]
        });
    },
    reloadTable: function () {
        curriculumItemIndex.table.ajax.reload(null, false);
    },
    initSelect2: function () {
        $(".select2-subjects").select2({
            placeholder: "Search Subjects",
            ajax: {
                url: "" + modal.controller + "/subjects",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.FullName
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });

        $(".select2-prerequisites").select2({
            placeholder: "Search for Pre-requisite Subjects",
            ajax: {
                url: "" + modal.controller + "/prerequisites",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.SubjectCode + " - " + item.DescriptiveTitle
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%",
            multiple: true
        });

        $(".select2-courses").select2({
            placeholder: "Search Courses",
            ajax: {
                url: "" + modal.controller + "/courses",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.Name
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });

        $(".select2-curriculumversions").select2({
            placeholder: "Search Curriculum",
            ajax: {
                url: "" + modal.controller + "/curriculumversions",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.ShortDescription
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });

        $(".select2-advisers").select2({
            placeholder: "Search Advisers",
            ajax: {
                url: "" + modal.controller + "/advisers",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.FullName
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });


        //$(".select2-rooms").select2({
        //    placeholder: "Search Rooms",
        //    ajax: {
        //        url: "" + modal.controller + "/rooms",
        //        dataType: 'json',
        //        data: function (params) {
        //            return {
        //                q: params.term
        //            };
        //        },
        //        processResults: function (data) {
        //            return {
        //                results: $.map(data, function (item) {
        //                    return {
        //                        id: item.RoomId,
        //                        text: item.RoomName
        //                    };
        //                })
        //            };
        //        },
        //        cache: true
        //    },
        //    dropdownParent: $("#app-modal .modal-content"),
        //    width: "100%"
        //});

    },
    initRepeater: function () {
        var repeaterIsEmpty = $(".repeater-is-empty").val() === "1" ? true : false;

        $(".schedule-repeater").repeater({
            initEmpty: repeaterIsEmpty,
            defaultValues: {
                "text-input": "foo"
            },
            ready: function () {
                $(".schedule-repeater").find(".schedule-selectpicker").selectpicker({
                    position: "bottom"
                });

                $(".schedule-repeater").find(".schedule-timepicker").timepicker();

                $(".select2-rooms").selectpicker("refresh");
            },
            show: function () {
                $(this).slideDown();

                $(this).find(".schedule-selectpicker").selectpicker({
                    position: "bottom"
                });

                $(this).find(".schedule-timepicker").timepicker();

                $(".select2-rooms").selectpicker("refresh");
            },
            hide: function (e) {
                $(this).slideUp(e);
            }
        });
    },
    callbacks: function () {
        curriculumItemIndex.initSelect2();
        curriculumItemIndex.initRepeater();
    }
};