﻿"use strict";

common.setActive("students");

$().ready(function () {
    studentsAdvisement.init();
});

var studentsAdvisement = {
    currentUserRole: null,
    studentId: null,
    advisedSubjects: [],
    subjectsToBeRemoved: [],
    alternativeSubject: [],
    showWarning: null,
    aggregateSelectedUnits: null,
    academicStanding: null,
    init: function () {
        studentsAdvisement.currentUserRole = common.currentUserRole();

        studentsAdvisement.registerEvents();
        studentsAdvisement.initStudentRecordSelect2();
        studentsAdvisement.initAcademicStandingSelect2();

        studentsAdvisement.studentId = $("#Student_Id").val();
        studentsAdvisement.academicStanding = $($(document).find("#academic-standing")).val();

        studentsAdvisement.aggregateSelectedUnits = 0;
        studentsAdvisement.checkAggregateSelectedUnits();

        modal.controller = "studentrecords";
        modal.headerText = "Student Record";
        modal.properties.createModalContentUrlParams = "?studentId=" + studentsAdvisement.studentId;

        modal.setProperties();

        modal.setController = function (e) {
            studentsAdvisement.setController(e);
            modal.setProperties();
        };

        modal.shownCallback = function () {
            studentsAdvisement.shownCallback();
        };

        modal.hiddenCallback = function (e) {
            studentsAdvisement.hiddenCallBack(e);
        };

        studentsAdvisement.initDataTable();
        modal.instances.dataTable = studentsAdvisement.studentRecordTable;

        modal.setup();
    },
    initStudentRecordSelect2: function () {
        $(document).find(".select2-final-grade").select2({
            width: "100%",
            placeholder: "Nothing Selected",
            dropdownParent: $("#app-modal .modal-content"),
            sorter: function (data) {
                return data.sort(function (a, b) {
                    return a.text < b.text ? -1 : a.text > b.text ? 1 : 0;
                });
            }
        });

        $(document).find(".select2-grade-status").select2({
            width: "100%",
            placeholder: "Nothing Selected",
            dropdownParent: $("#app-modal .modal-content"),
            sorter: function (data) {
                return data.sort(function (a, b) {
                    return a.text < b.text ? -1 : a.text > b.text ? 1 : 0;
                });
            }
        });
    },
    initAcademicStandingSelect2: function () {
        $(document).find(".select2-academic-standing").select2({
            width: "100%",
            placeholder: "Nothing Selected",
            dropdownParent: $("#app-modal .modal-content")
        });
    },
    checkAggregateSelectedUnits: function () {
        $(document).find(".advised-subjects-checkbox").each(function (index, element) {
            var thisUnits = $(element).data("units");

            studentsAdvisement.aggregateSelectedUnits += thisUnits;
        });
    },
    checkAdvisedSubjectsArray: function () {
        if (studentsAdvisement.advisedSubjects.length > 0) {
            $("#add-advised-subjects").show();
        }
        else {
            $("#add-advised-subjects").hide();
        }
    },
    checkSubjectsToBeRemovedArray: function () {
        if (studentsAdvisement.subjectsToBeRemoved.length > 0) {
            $("#delete-advised-subjects").show();
        }
        else {
            $("#delete-advised-subjects").hide();
        }
    },
    checkAlternativeSubjectArray: function () {
        if (studentsAdvisement.alternativeSubject.length === 0) {
            modal.blockPost = true;
            modal.blockPostErrorMessage = "Please select one subject";
        }
        else {
            modal.blockPost = false;
        }
    },
    registerEvents: function () {
        $(document).on("select2:select", ".select2-final-grade", function (e) {
            var data = e.params.data;
            var finalgrade = parseInt($(e.target).val());

            if (finalgrade >= 1 && finalgrade < 10) {
                $(document).find(".select2-grade-status").val("1");
            } else if (finalgrade === 10) {
                $(document).find(".select2-grade-status").val("5");
            } else if (finalgrade === 11) {
                $(document).find(".select2-grade-status").val("3");
            } else if (finalgrade === 12) {
                $(document).find(".select2-grade-status").val("2");
            } else {
                $(document).find(".select2-grade-status").val("4");
            }

            $(document).find(".select2-grade-status").select2().trigger("change");
        });

        $(document).on("select2:select", ".select2-academic-standing", function (e) {
            studentsAdvisement.academicStanding = $("#AcademicStanding option:selected").text();
        });

        $(document).on("click", "#add-advised-subjects", function (e) {
            e.preventDefault();
            if (studentsAdvisement.advisedSubjects.length === 0) {
                swal.fire({
                    title: "Error",
                    text: "Cannot add subjects on the advised subjects table because there are no subjects selected in the prospectus table",
                    type: "error",
                    showCancelButton: !0,
                    confirmButtonText: "Okay"
                }).then(function (e) {
                    e.value;
                });
            }
            else {
                if (studentsAdvisement.showWarning) {
                    swal.fire({
                        title: "Warning",
                        text: "Some of the selected subjects might not be available this semester or is not prescribed according to your year level. Please proceed with caution",
                        type: "warning",
                        showCancelButton: !0,
                        confirmButtonText: "Confirm"
                    }).then(function (e) {
                        if (e.value) {
                            swal.fire({
                                title: "Add subjects?",
                                text: "Selected subjects will be added on the advised subjects list",
                                type: "question",
                                showCancelButton: !0,
                                confirmButtonText: "Yes, add it!"
                            }).then(function (f) {
                                f.value && studentsAdvisement.saveSelectedSubjects();
                            });
                        }
                    });
                }
                else {
                    // repeated code
                    swal.fire({
                        title: "Add subjects?",
                        text: "Selected subjects will be added on the advised subjects list",
                        type: "question",
                        showCancelButton: !0,
                        confirmButtonText: "Yes, add it!"
                    }).then(function (f) {
                        f.value && studentsAdvisement.saveSelectedSubjects();
                    });
                }
            }
        });

        $(document).on("click", "#delete-advised-subjects", function (e) {
            e.preventDefault();

            swal.fire({
                title: "Remove subjects?",
                text: "Selected subjects will be removed on the advised subjects list",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Yes, remove it!"
            }).then(function (e) {
                e.value && studentsAdvisement.deleteSelectedSubjects();
            });
        });

        $(document).on("click", "#finalize-advised-subjects", function (e) {
            e.preventDefault();

            if ($(".advised-subjects-table .danger-highlight").length) {
                swal.fire({
                    title: "Error",
                    text: "Cannot finalize advisement because of schedule conflict",
                    type: "error",
                    showCancelButton: !0,
                    confirmButtonText: "Got it"
                }).then(function (e) {
                    e.value;
                });
            } else if ($(".advised-subjects-checkbox").length === 0) {
                swal.fire({
                    title: "Error",
                    text: "Cannot finalize advisement because there are no subjects in the list",
                    type: "error",
                    showCancelButton: !0,
                    confirmButtonText: "Okay"
                }).then(function (e) {
                    e.value;
                });
            } else {
                swal.fire({
                    title: "Finalize advisement?",
                    text: "Selected subjects will be added to the student records list",
                    type: "question",
                    showCancelButton: !0,
                    confirmButtonText: "Confirm"
                }).then(function (e) {
                    e.value && studentsAdvisement.finalizeAdvisedSubjects();
                });
            }
        });


        //$("#print-advised-subjects").click(function (e) {
        //    e.preventDefault();

        //    swal.fire("Attention!", "Feature not yet implemented", "warning");
        //});

        $(document).on("change", ".prospectus-checkbox", function (e) {
            var checkedValue = $(e.target).val();
            var isChecked = $(e.target).is(":checked");
            var shouldTriggerWarning = $(e.target).data("trigger-warning");

            if (isChecked) {
                var units = $(e.target).data("units");
                var allowedUnits = $($(document).find("#allowed-units")[0]).val();

                studentsAdvisement.aggregateSelectedUnits += units;

                // check for aggregate count if it exceeds allowed units
                // do not push the value
                // remove the checked flag

                if (studentsAdvisement.aggregateSelectedUnits > allowedUnits) {
                    $(e.target).prop("checked", false);

                    swal.fire("Attention!", "Cannot select more subjects because the total units selected is greater than the maximum allowed units", "warning");

                    return false;
                }

                studentsAdvisement.advisedSubjects.push(checkedValue);

                if (shouldTriggerWarning === true) {
                    studentsAdvisement.showWarning = true;
                }
            }
            else {
                studentsAdvisement.advisedSubjects.splice(studentsAdvisement.advisedSubjects.indexOf(checkedValue), 1);
            }

            //studentsAdvisement.checkAdvisedSubjectsArray();
        });

        $(document).on("change", ".advised-subjects-checkbox", function (e) {
            var checkedValue = $(e.target).val();
            var isChecked = $(e.target).is(":checked");

            if (isChecked) {
                studentsAdvisement.subjectsToBeRemoved.push(checkedValue);
            }
            else {
                studentsAdvisement.subjectsToBeRemoved.splice(studentsAdvisement.subjectsToBeRemoved.indexOf(checkedValue), 1);
            }

            studentsAdvisement.checkSubjectsToBeRemovedArray();
        });

        $(document).on("change", ".alternative-subjects-checkbox", function (e) {
            $(".alternative-subjects-checkbox").each(function (index, element) {
                if ($(element).is(":checked")) {
                    if ($(element)[0] !== $(e.target)[0]) {
                        $(element).prop("checked", false);
                    }
                }
            });

            studentsAdvisement.alternativeSubject = [];

            var checkedValue = $(e.target).val();
            var checkedValueSection = $(e.target).data("section");
            var isChecked = $(e.target).is(":checked");

            if (isChecked) {
                studentsAdvisement.alternativeSubject.push(checkedValue);

                $("#AlternativeSubjectCurriculumItemId").val(checkedValue);
                $("#AlternativeSubjectSection").val(checkedValueSection);
            }

            studentsAdvisement.checkAlternativeSubjectArray();
        });
    },
    setController: function (e) {
        var controller = $(e.relatedTarget).data("controller");
        var entityId = $(e.relatedTarget).data("id");

        switch (controller) {
            case "enrollmenthistory":
                modal.controller = "enrollmenthistory";
                modal.headerText = "Enrollment History";
                break;
            case "students":
                modal.controller = "students";
                modal.headerText = "Student";
                break;
            case "advisedsubjects":
                modal.controller = "advisedsubjects";
                modal.headerText = "Advised Subject";
                break;
            case "curriculumitems":
                modal.controller = "curriculumitems";
                modal.properties.customModalDataUrl = "/curriculumitems/alternativesubjects/" + entityId;
                modal.properties.customModalTitle = "Alternative Subjects";
                modal.properties.customModalButtonTitle = "Save";
                modal.properties.customModalPostUrl = "/advisedsubjects/update";
                break;
            default:
                modal.controller = "studentrecords";
                modal.headerText = "Student Record";
                break;
        }
    },
    studentRecordTable: null,
    enrollmentHistoryTable: null,
    prospectusTable: null,
    advisedSubjectsTable: null,
    outstandingSubjectsTable: null,
    alternativeSubjectsTable: null,
    initDataTable: function () {
        studentsAdvisement.studentRecordTable = $(".student-record-table").DataTable({
            responsive: true,
            order: [0, "asc"],
            ajax: {
                url: "/" + modal.controller + "/list/" + studentsAdvisement.studentId,
                dataSrc: ""
            }, columns: [
                {
                    data: "AcademicTerm",
                    orderable: false,
                    visible: false
                },
                {
                    data: "Id",
                    render: function (data, type, row) {

                        dataTable.dataTableOptions.showAlternatives = false;
                        dataTable.dataTableOptions.showEdit = false;
                        dataTable.dataTableOptions.showEditEvaluation = row.Id !== null && studentsAdvisement.currentUserRole === "Adviser";
                        dataTable.dataTableOptions.showDetails = false;
                        dataTable.dataTableOptions.showNothing = row.Id === null || studentsAdvisement.currentUserRole !== "Adviser";
                        dataTable.dataTableOptions.showDelete = false;
                        dataTable.dataTableOptions.showEditOutstandingSubject = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "SubjectCode",
                    orderable: false
                },
                {
                    data: "DescriptiveTitle",
                    orderable: false
                },
                {
                    data: "PreRequisites",
                    orderable: false
                },
                {
                    data: "FinalGrade",
                    orderable: false
                },
                {
                    data: "Status",
                    orderable: false
                }
            ],
            rowGroup: {
                dataSrc: "AcademicTerm"
            },
            dom: "t",
            paging: false
        });

        studentsAdvisement.enrollmentHistoryTable = $(".enrollment-history-table").DataTable({
            responsive: true,
            order: [0, "desc"],
            ajax: {
                url: "/" + modal.controller + "/enrollmenthistory/" + studentsAdvisement.studentId,
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    orderable: false,
                    visible: false
                },
                {
                    data: "AcademicTerm",
                    orderable: false
                },
                {
                    data: "Year",
                    orderable: false
                },
                {
                    data: "AcademicStanding",
                    orderable: false
                },
                {
                    data: "Status",
                    orderable: false
                },
                {
                    data: "EnrollmentStatus",
                    orderable: false
                }
            ],
            dom: "t"
        });

        if ($(".prospectus-table")) {
            studentsAdvisement.initProspectusTable();
        }

        if ($(".advised-subjects-table")) {
            studentsAdvisement.initAdvisedSubjectsTable();
        }

        if ($(".outstanding-subjects-table")) {
            studentsAdvisement.initOutstandingSubjectsTable();
        }
    },
    initProspectusTable: function () {
        studentsAdvisement.prospectusTable = $(".prospectus-table").DataTable({
            responsive: true,
            order: [0, "asc"],
            ajax: {
                url: "/studentrecords/prospectus/" + studentsAdvisement.studentId,
                dataSrc: ""
            }, columns: [
                {
                    data: "AcademicTerm",
                    orderable: false,
                    visible: false
                },
                {
                    data: "IsAvailable",
                    orderable: false,
                    visible: false
                },
                {
                    data: "Status",
                    orderable: false,
                    visible: false
                },
                {
                    data: "Id",
                    render: function (data, type, row) {
                        if (row.IsAvailable === true) {
                            if (row.IsAdvised === true) {
                                return `<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" name="AdvisedSubjects" value="` + data + `" checked disabled class="prospectus-checkbox">
                                            <span></span>
                                        </label>`;
                            }
                            else if (!row.IsSemesterMatching || !row.IsYearMatching) {
                                return `<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" data-units="` + row.Units + `" data-trigger-warning="true" name="AdvisedSubjects" value="` + data + `" class="prospectus-checkbox">
                                            <span></span>
                                        </label>`;
                            }
                            else {
                                return `<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                            <input type="checkbox" data-units="` + row.Units + `" data-trigger-warning="false" name="AdvisedSubjects" value="` + data + `" class="prospectus-checkbox">
                                            <span></span>
                                        </label>`;
                            }
                        }
                        else {
                            return "";
                        }
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "SubjectCode",
                    orderable: false
                },
                {
                    data: "DescriptiveTitle",
                    orderable: false
                },
                {
                    data: "Units",
                    orderable: false
                },
                {
                    data: "PreRequisites",
                    orderable: false
                }
            ],
            rowGroup: {
                dataSrc: "AcademicTerm"
            },
            dom: "t",
            paging: false,
            createdRow: function (row, data, index) {
                if (data["IsAvailable"] !== true) {

                    if (data["Status"] === "Passed") {
                        $(row).addClass("passed-highlight");
                    }
                    else {
                        $(row).addClass("highlight");
                    }
                    $(row).attr("data-toggle", "kt-popover").attr("data-placement", "left").attr("data-content", data["Status"]).attr("data-trigger", "hover").attr("data-html", "true");
                }

                if (data["Status"] === "Failed") {
                    $(row).addClass("danger-highlight");
                    $(row).attr("data-toggle", "kt-popover").attr("data-placement", "left").attr("data-content", data["Status"]).attr("data-trigger", "hover").attr("data-html", "true");
                }

                if (data["IsSemesterMatching"] !== true && data["IsYearMatching"] === true) {
                    if (!$(row).hasClass("passed-highlight")) {
                        if (!$(row).hasClass("highlight") && !$(row).hasClass("danger-highlight")) {
                            $(row).addClass("soft-highlight");
                            $(row).attr("data-toggle", "kt-popover").attr("data-placement", "left").attr("data-content", data["SemesterMatchMessage"]).attr("data-trigger", "hover").attr("data-html", "true");
                        }
                    }
                }

                if (data["IsYearMatching"] !== true && data["IsSemesterMatching"] !== true) {
                    if (!$(row).hasClass("passed-highlight")) {
                        if (!$(row).hasClass("highlight") && !$(row).hasClass("danger-highlight")) {
                            $(row).addClass("soft-highlight");
                            $(row).attr("data-toggle", "kt-popover").attr("data-placement", "left").attr("data-content", data["YearMatchMessage"] + "<br />" + data["SemesterMatchMessage"]).attr("data-trigger", "hover").attr("data-html", "true");
                        }
                    }
                }

                if (data["IsYearMatching"] !== true && data["IsSemesterMatching"] === true) {
                    if (!$(row).hasClass("passed-highlight")) {
                        if (!$(row).hasClass("highlight") && !$(row).hasClass("danger-highlight")) {
                            $(row).addClass("soft-highlight");
                            $(row).attr("data-toggle", "kt-popover").attr("data-placement", "left").attr("data-content", data["YearMatchMessage"]).attr("data-trigger", "hover").attr("data-html", "true");
                        }
                    }
                }
            },
            drawCallback: function () {
                $('[data-toggle="kt-popover"]').popover();
            }
        });
    },
    initAdvisedSubjectsTable: function () {
        studentsAdvisement.advisedSubjectsTable = $(".advised-subjects-table").DataTable({
            responsive: true,
            order: [0, "asc"],
            ajax: {
                url: "/advisedsubjects/list/" + studentsAdvisement.studentId,
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    orderable: false,
                    visible: false
                },
                {
                    data: "Id",
                    render: function (data, type, row, meta) {
                        return `<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input type="checkbox" data-units="` + row.Units + `" name="AdvisedSubjects" value="` + data + `" class="advised-subjects-checkbox">
                                    <span></span>
                                </label>
                                <input type="hidden" name="model[` + meta.row + `].CurriculumItemId" value="` + row.CurriculumItemId + `">
                                <input type="hidden" name="model[` + meta.row + `].Grade.Status" value="4">
                                <input type="hidden" name="model[` + meta.row + `].StudentId" value="` + studentsAdvisement.studentId + `">`
                            ;
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "Id",
                    render: function (data) {
                        var isRegular = studentsAdvisement.academicStanding === "Regular";

                        dataTable.dataTableOptions.showAlternatives = !isRegular;
                        dataTable.dataTableOptions.showDetails = false;
                        dataTable.dataTableOptions.showDelete = false;
                        dataTable.dataTableOptions.showEdit = false;
                        dataTable.dataTableOptions.showNothing = isRegular;
                        dataTable.dataTableOptions.showEditOutstandingSubject = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "SubjectCode",
                    orderable: false
                },
                {
                    data: "DescriptiveTitle",
                    orderable: false
                },
                {
                    data: "YearCourseSection",
                    orderable: false
                },
                {
                    data: "Units",
                    orderable: false
                },
                {
                    data: "PreRequisites",
                    orderable: false
                }
            ],
            dom: "t",
            paging: false,
            createdRow: function (row, data, index) {
                if (data["HasConflictingSchedule"] === true) {
                    $(row).addClass("danger-highlight");
                    $(row).attr("data-toggle", "kt-popover").attr("data-placement", "left").attr("data-content", "In conflict with:<br />" + data["ConflictingSubjects"]).attr("data-trigger", "hover").attr("data-html", "true");
                }
            },
            drawCallback: function () {
                $('[data-toggle="kt-popover"]').popover();

                studentsAdvisement.aggregateSelectedUnits = 0;
                studentsAdvisement.checkAggregateSelectedUnits();
            }
        });
    },
    initAlternativeSubjectsTable: function () {
        studentsAdvisement.alternativeSubjectsTable = $(".alternative-subjects-table").DataTable({
            responsive: true,
            order: [0, "asc"],
            ajax: {
                url: "/curriculumitems/alternativesubjectslist/" + $(".alternative-subject-reference-id").val(),
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    orderable: false,
                    visible: false
                },
                {
                    data: "CurriculumItemId",
                    render: function (data, type, row, meta) {
                        return `<label class="kt-checkbox kt-checkbox--single kt-checkbox--solid">
                                    <input type="checkbox" data-curriculum-item-id="` + row.CurriculumItemId + `" data-section="` + row.Section + `" name="AlternativeSubjects" value="` + data + `" class="alternative-subjects-checkbox">
                                    <span></span>
                                </label>`
                            ;
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showAlternatives = true;
                        dataTable.dataTableOptions.showDetails = true;
                        dataTable.dataTableOptions.showDelete = false;
                        dataTable.dataTableOptions.showEdit = false;
                        dataTable.dataTableOptions.showNothing = false;
                        dataTable.dataTableOptions.showEditOutstandingSubject = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "SubjectCode",
                    orderable: false
                },
                {
                    data: "DescriptiveTitle",
                    orderable: false
                },
                {
                    data: "YearCourseSection",
                    orderable: false
                },
                {
                    data: "Units",
                    orderable: false
                },
                {
                    data: "PreRequisites",
                    orderable: false
                }
            ],
            dom: "t",
            paging: false
        });
    },
    initOutstandingSubjectsTable: function () {
        studentsAdvisement.outstandingSubjectsTable = $(".outstanding-subjects-table").DataTable({
            responsive: true,
            order: [0, "asc"],
            ajax: {
                url: "/students/outstandingsubjects/" + studentsAdvisement.studentId,
                dataSrc: ""
            }, columns: [
                {
                    data: "AcademicTerm",
                    orderable: false,
                    visible: false
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showAlternatives = false;
                        dataTable.dataTableOptions.showDetails = false;
                        dataTable.dataTableOptions.showDelete = false;
                        dataTable.dataTableOptions.showEdit = false;
                        dataTable.dataTableOptions.showEditOutstandingSubject = true;
                        dataTable.dataTableOptions.showNothing = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                {
                    data: "SubjectCode",
                    orderable: false
                },
                {
                    data: "DescriptiveTitle",
                    orderable: false
                },
                {
                    data: "PreRequisites",
                    orderable: false
                },
                {
                    data: "FinalGrade",
                    orderable: false
                },
                {
                    data: "Status",
                    orderable: false
                }
            ],
            dom: "t",
            paging: false,
            drawCallback: function () {
                //if (studentsAdvisement.outstandingSubjectsTable !== null) {
                //    var rowCount = studentsAdvisement.outstandingSubjectsTable.data().length;

                //    if (rowCount === 0) {
                //        studentsAdvisement.loadOutstandingSubjectsTabContent();
                //    }
                //}
            }
        });
    },
    reloadTable: function () {
        studentsAdvisement.studentRecordTable.ajax.reload(null, false);
        studentsAdvisement.enrollmentHistoryTable.ajax.reload(null, false);

        if ($(".prospectus-table").length) {
            studentsAdvisement.prospectusTable.ajax.reload(null, false);
        }

        if ($(".advised-subjects-table").length) {
            studentsAdvisement.advisedSubjectsTable.ajax.reload(null, false);
        }

        if ($(".outstanding-subjects-table").length) {
            studentsAdvisement.outstandingSubjectsTable.ajax.reload(null, false);
        }
    },
    initSelect2: function () {
        $(".select2-curriculumitems").select2({
            placeholder: "Search Subjects",
            ajax: {
                url: "/" + modal.controller + "/curriculumitems/" + studentsAdvisement.studentId,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.FullName
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });

        $(".select2-academicterms").select2({
            placeholder: "Search Academic Term",
            ajax: {
                url: "/" + modal.controller + "/academicterms",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.FullName
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });

        $(".select2-enrollmenthistory").select2({
            placeholder: "Search Enrolled Academic Term",
            ajax: {
                url: "/" + modal.controller + "/enrollmenthistory/" + studentsAdvisement.studentId,
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.AcademicTerm
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });


        $(".select2-advisers").select2({
            placeholder: "Search Advisers",
            ajax: {
                url: "/" + modal.controller + "/advisers/",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.FullName
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });
    },
    shownCallback: function () {
        studentsAdvisement.initSelect2();

        if ($(".alternative-subjects-table").length) {
            studentsAdvisement.initAlternativeSubjectsTable();
            studentsAdvisement.checkAlternativeSubjectArray();
        }
    },
    loadAdvisementTabContent: function () {
        fetch("/students/advisementtab/" + studentsAdvisement.studentId, {
            method: "GET"
        }).then(response => {
            return response.text();
        }).then(content => {
            $(".advisement-tab-contents").html(content);

            studentsAdvisement.initProspectusTable();
            studentsAdvisement.initAdvisedSubjectsTable();
        });
    },
    loadAdvisementabDetailsContent: function () {
        fetch("/students/advisementtabdetails/" + studentsAdvisement.studentId, {
            method: "GET"
        }).then(response => {
            return response.text();
        }).then(content => {
            $(".advisement-tab-details").html(content);
        });
    },
    loadOutstandingSubjectsTabContent: function () {
        fetch("/students/outstandingsubjectstab/" + studentsAdvisement.studentId, {
            method: "GET"
        }).then(response => {
            return response.text();
        }).then(content => {
            $(".outstanding-subjects-tab-content").html(content);

            studentsAdvisement.initOutstandingSubjectsTable();
        });
    },
    hiddenCallBack: function (e) {
        var action = modal.action;
        var controller = modal.controller;

        modal.blockPost = false;

        studentsAdvisement.reloadTable();

        studentsAdvisement.updateEnrollment().then(() => {
            if (action === "edit-outstanding-subjects" || (action === "edit" && controller === "studentrecords")) {
                studentsAdvisement.loadAdvisementTabContent();

                if (studentsAdvisement.outstandingSubjectsTable !== null) {
                    var rowCount = studentsAdvisement.outstandingSubjectsTable.data().length;

                    if (rowCount <= 1) {
                        studentsAdvisement.loadOutstandingSubjectsTabContent();
                    }
                }
            }
        });

        if (action === "add" && controller === "enrollmenthistory") {
            studentsAdvisement.loadAdvisementTabContent();
        }
        else if (action === "edit" && controller === "enrollmenthistory") {
            studentsAdvisement.loadAdvisementabDetailsContent();
        }
        else if (action === "edit" && controller === "students") {
            studentsAdvisement.loadAdvisementabDetailsContent();
        }
        //else if (action === "edit-outstanding-subjects") {
        //    if (studentsAdvisement.outstandingSubjectsTable !== null) {
        //        var rowCount = studentsAdvisement.outstandingSubjectsTable.data().length;

        //        if (rowCount <= 1) {
        //            studentsAdvisement.loadOutstandingSubjectsTabContent();
        //        }
        //    }
        //}
    },
    saveSelectedSubjects: function () {
        var advisedSubjectsFormData = new FormData(document.getElementById("add-advised-subjects-form"));

        for (var i = 0; i < studentsAdvisement.advisedSubjects.length; i++) {
            advisedSubjectsFormData.append("AdvisedSubjectIds[]", studentsAdvisement.advisedSubjects[i]);
        }

        fetch("/advisedsubjects/save", {
            method: "POST",
            body: advisedSubjectsFormData
        }).then(response => {
            return response.json();
        }).then(result => {
            swal.fire("Success!", "All subjects were added to the list!", "success");
            studentsAdvisement.advisedSubjectsTable.ajax.reload(null, false);
            studentsAdvisement.prospectusTable.ajax.reload(null, false);

            studentsAdvisement.advisedSubjects = [];
        }).then(() => {
            //studentsAdvisement.checkAdvisedSubjectsArray();
            studentsAdvisement.checkSubjectsToBeRemovedArray();
            studentsAdvisement.aggregateSelectedUnits = 0;

            studentsAdvisement.checkAggregateSelectedUnits();
            studentsAdvisement.showWarning = false;
        });
    },
    deleteSelectedSubjects: function () {
        var advisedSubjectsFormData = new FormData();

        for (var i = 0; i < studentsAdvisement.subjectsToBeRemoved.length; i++) {
            advisedSubjectsFormData.append("ids[]", studentsAdvisement.subjectsToBeRemoved[i]);
        }

        fetch("/advisedsubjects/confirmdelete", {
            method: "POST",
            body: advisedSubjectsFormData
        }).then(response => {
            return response.json();
        }).then(result => {
            swal.fire("Success!", "All subjects were removed from the list!", "success");
            studentsAdvisement.advisedSubjectsTable.ajax.reload(null, false);
            studentsAdvisement.prospectusTable.ajax.reload(null, false);

            studentsAdvisement.subjectsToBeRemoved = [];
        }).then(() => {
            //studentsAdvisement.checkAdvisedSubjectsArray();
            studentsAdvisement.checkSubjectsToBeRemovedArray();
        });
    },
    finalizeAdvisedSubjects: function () {
        var advisedSubjectsTableFormData = new FormData(document.getElementById("advised-subjects-table-form"));

        fetch("/studentrecords/finalizeadvisement", {
            method: "POST",
            body: advisedSubjectsTableFormData
        }).then(response => {
            return response.json();
        }).then(result => {
            swal.fire("Success!", "All subjects were added to the student record!", "success");
            studentsAdvisement.advisedSubjectsTable.ajax.reload(null, false);
            studentsAdvisement.prospectusTable.ajax.reload(null, false);

            studentsAdvisement.subjectsToBeRemoved = [];
        }).then(() => {
            studentsAdvisement.loadOutstandingSubjectsTabContent();
            studentsAdvisement.loadAdvisementTabContent();

            studentsAdvisement.studentRecordTable.ajax.reload(null, false);
            studentsAdvisement.enrollmentHistoryTable.ajax.reload(null, false);

            //studentsAdvisement.checkAdvisedSubjectsArray();
            studentsAdvisement.checkSubjectsToBeRemovedArray();
        });
    },
    updateEnrollment: function () {
        return fetch("/studentrecords/updateenrollment/" + studentsAdvisement.studentId, {
            method: "GET"
        }).then(response => {
            return response.json();
        }).then(result => {
            studentsAdvisement.enrollmentHistoryTable.ajax.reload(null, false);
        });
    }
};