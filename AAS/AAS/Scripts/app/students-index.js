﻿"use strict";

common.setActive("students");

$().ready(function () {
    app.init();
});

var app = {
    init: function () {
        modal.controller = "students";
        modal.headerText = "student";

        modal.setProperties();

        app.initDataTable();

        modal.instances.dataTable = app.table;

        modal.hiddenCallback = function () {
            app.reloadTable();
        };

        modal.setup();
    },
    table: null,
    initDataTable: function () {
        app.table = $(".table").DataTable({
            responsive: true,
            order: [0, "desc"],
            ajax: {
                url: "/" + modal.controller + "/list",
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    visible: false,
                    orderable: false
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showDetails = false;
                        dataTable.dataTableOptions.showAdvisement = true;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                { data: "IdNumber" },
                { data: "FullName" },
                { data: "Gender" },
                { data: "Year" },
                { data: "Course" },
                { data: "AcademicStanding" },
                { data: "Email" },
                { data: "ContactNumber" }
            ]
        });
    },
    reloadTable: function () {
        app.table.ajax.reload(null, false);
    }
};