﻿"use strict";

common.setActive("subjects");

$().ready(function () {
    app.init();
});

var app = {
    init: function () {
        modal.controller = "subjects";
        modal.headerText = "subject";

        modal.setProperties();

        app.initDataTable();
        modal.instances.dataTable = app.table;

        modal.hiddenCallback = function () {
            app.reloadTable();
        };

        modal.shownCallback = function () {
            app.callbacks();
        };

        modal.setup();
    },
    table: null,
    initDataTable: function () {
        app.table = $(".table").DataTable({
            responsive: true,
            order: [0, "desc"],
            ajax: {
                url: "/" + modal.controller + "/list",
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    visible: false,
                    orderable: false
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showDelete = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                { data: "SubjectCode" },
                { data: "DescriptiveTitle" },
                { data: "Units" }
            ]
        });
    },
    reloadTable: function () {
        app.table.ajax.reload(null, false);
    },
    initSelect2: function () {
        $(".select2-advisers").select2({
            placeholder: "Search advisers",
            ajax: {
                url: "" + modal.controller + "/advisers",
                dataType: 'json',
                data: function (params) {
                    return {
                        q: params.term
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                id: item.Id,
                                text: item.FullName
                            };
                        })
                    };
                },
                cache: true
            },
            dropdownParent: $("#app-modal .modal-content"),
            width: "100%"
        });
    },
    callbacks: function () {
        app.initSelect2();
    }
};