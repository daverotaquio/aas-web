﻿"use strict";

//common.setActive("advisers");

$().ready(function () {
    advisersIndex.init();
});

var advisersIndex = {
    init: function () {
        modal.controller = "users";
        modal.headerText = "user";

        modal.setProperties();

        advisersIndex.initDataTable();
        modal.instances.dataTable = advisersIndex.table;

        modal.hiddenCallback = function () {
            advisersIndex.reloadTable();
        };

        modal.setup();
    },
    table: null,
    initDataTable: function () {
        advisersIndex.table = $(".table").DataTable({
            responsive: true,
            order: [0, "desc"],
            ajax: {
                url: "/" + modal.controller + "/list",
                dataSrc: ""
            }, columns: [
                {
                    data: "Id",
                    visible: false,
                    orderable: false
                },
                {
                    data: "Id",
                    render: function (data) {
                        dataTable.dataTableOptions.showDelete = false;

                        dataTable.setDataTableAction(data);

                        return dataTable.dataTableAction;
                    },
                    orderable: false,
                    className: "td-center"
                },
                { data: "FullName" },
                { data: "User.Email" },
                { data: "ContactNumber" },
                { data: "Roles" }
            ]
        });
    },
    reloadTable: function () {
        advisersIndex.table.ajax.reload(null, false);
    }
};