﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AAS.Startup))]
namespace AAS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
